<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductlistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productlists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->integer('brand_id')->nullable();
            $table->integer('label_id')->nullable();
            $table->integer('Categorie_id')->nullable();
            $table->string('picture')->nullable();
            $table->string('short_description')->nullable();
            $table->string('description')->nullable();
            $table->integer('total_sales')->nullable();
            $table->tinyInteger('is_new')->nullable();
            $table->float('cost')->nullable();
            $table->float('mrp')->nullable();
            $table->float('special_price')->nullable();
            $table->tinyInteger('is_draft')->nullable();
            $table->tinyInteger('is_active')->nullable();
            $table->tinyInteger('soft_delete')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productlists');
    }
}
