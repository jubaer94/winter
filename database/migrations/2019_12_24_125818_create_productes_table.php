<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('tyoe');
            $table->string('name')->nullable();
            $table->string('slug')->nullable();
            $table->string('sku')->nullable();
            $table->string('barcode')->nullable();
            $table->text('description')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->tinyInteger('in_stock')->nullable();
            $table->tinyInteger('track_stock')->nullable();
            $table->decimal('qty')->nullable();
            $table->tinyInteger('is_taxable')->nullable();
            $table->decimal('price')->nullable();
            $table->decimal('cost_price')->nullable();
            $table->double('weight')->nullable();
            $table->double('height')->nullable();
            $table->double('length')->nullable();
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productes');
    }
}
