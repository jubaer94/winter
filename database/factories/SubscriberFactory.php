<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\subscriber;
use Faker\Generator as Faker;

$factory->define(subscriber::class, function (Faker $faker) {
    return [
        'email'=>$faker->email,
        'is_subscribed'=>$faker->boolean,
        'reason'=>$faker->text(200),

    ];
});
