<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\contact;
use Faker\Generator as Faker;

$factory->define(contact::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'email'=>$faker->email,
        'subject'=>$faker->sentence,
        'comment'=>$faker->sentence(12),
        'status'=>$faker->boolean,
        'soft_delete'=>$faker->boolean,
    ];
});
