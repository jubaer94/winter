<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\order;
use Faker\Generator as Faker;

$factory->define(order::class, function (Faker $faker) {
    return [
        'product_id'=>$faker->numberBetween(1,50),
        'qty'=>$faker->numberBetween(1,20),

    ];
});
