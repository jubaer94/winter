<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\map_product_tag;
use Faker\Generator as Faker;

$factory->define(map_product_tag::class, function (Faker $faker) {
    return [
        'product_id'=>$faker->numberBetween(1,50),
        'tag_id'=>$faker->numberBetween(1,20),
    ];
});
