<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\label;
use Faker\Generator as Faker;

$factory->define(label::class, function (Faker $faker) {
    return [
        'picture'=>$faker->randomElement(['category_1.png','category_2.png','category_3.png','category_4.png','category_5.png','category_6.png','category_7.png']),
        'title'=>$faker->word,
    ];
});
