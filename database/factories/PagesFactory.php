<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\page;
use Faker\Generator as Faker;

$factory->define(page::class, function (Faker $faker) {
    return [
        'title'=>$faker->title,
        'description'=>$faker->text(200),
        'link'=>$faker->url ,
    ];
});
