<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\admin;
use Faker\Generator as Faker;

$factory->define(admin::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'email'=>$faker->email,
        'password'=>$faker->password,
        'phone'=>$faker->phoneNumber,
        'soft_delete'=>$faker->boolean,
        'is_draft'=>$faker->boolean,
    ];
});
