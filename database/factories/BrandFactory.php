<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Brand;
use Faker\Generator as Faker;

$factory->define(Brand::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'title'=>$faker->company,
        'link'=>$faker->url,
        'is_active'=>$faker->boolean,
        'is_draft'=>$faker->boolean,
        'soft_delete'=>$faker->boolean,
    ];
});
