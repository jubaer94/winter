<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\banner;
use Faker\Generator as Faker;

$factory->define(banner::class, function (Faker $faker) {
    return [
         'title'=>$faker->company,
        'picture'=>$faker->randomElement(['1580274560__Akshay-Kumar-Bachchan-Pandey.jpg','prodect_details_1.png','prodect_details_2.png','prodect_details_3.png']),
        'link'=> $faker->url,
        'promotional_message' => $faker->text(50),
        'html_banner' => $faker->text(100),
        'is_active' => $faker->boolean,
        'is_draft'=>$faker->boolean,
        'soft_delete'=>$faker->boolean,
        'max_display'=>$faker->numberBetween(1,10),




    ];
});
