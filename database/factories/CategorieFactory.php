<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Categorie;
use Faker\Generator as Faker;

$factory->define(Categorie::class, function (Faker $faker) {
    return [
        'name'=>$faker->name,
        'link'=>$faker->url,
        'picture'=>$faker->randomElement(['category_1.png','category_2.png','category_3.png','category_4.png','category_5.png','category_6.png','category_7.png','category_8.png','category_9.png','category_10.png','category_11.png','category_12.png']),
        'soft_delete'=>$faker->boolean,
        'is_draft'=>$faker->boolean,

    ];
});
