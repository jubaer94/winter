<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\productlist;
use Faker\Generator as Faker;

$factory->define(productlist::class, function (Faker $faker) {
    return [
        'name'=>$faker->company,
        'title'=>$faker->title,
        'brand_id'=>$faker->numberBetween(1,20),
        'label_id'=>$faker->numberBetween(1,10),
        'picture'=>$faker->randomElement(['arrivel_1.png','arrivel_2.png','arrivel_3.png','arrivel_4.png','arrivel_5.png','arrivel_6.png','review-1.png','review-2.png','review-3.png']),
        'short_description'=>$faker->sentence(20),
        'description'=>$faker->text(200),
        'total_sales'=>$faker->numberBetween(1,1000),
        'is_new'=>$faker->boolean,
        'cost'=>$faker->numberBetween(1,10000),
        'mrp'=>$faker->numberBetween(1,20000),
        'special_price'=>$faker->numberBetween(1,15000),
        'is_active' => $faker->boolean,
        'is_draft'=>$faker->boolean,
        'soft_delete'=>$faker->boolean,
        'Categorie_id'=>$faker->numberBetween(1,10),

    ];
});
