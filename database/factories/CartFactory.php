<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\cart;
use Faker\Generator as Faker;

$factory->define(cart::class, function (Faker $faker) {
    return [
        'sid'=>$faker->numberBetween(1,100),
        'product_id'=>$faker->numberBetween(1,50),
        'picture'=>'1580712245__Akshay-Kumar-Bachchan-Pandey.jpg',
        'product_title' => $faker->title,
        'qty'=>$faker->numberBetween(1,100),
        'unite_price'=>$faker->numberBetween(1,10000),
        'total_price'=>$faker->numberBetween(1,100000)

    ];
});
