<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\popular_tag;
use Faker\Generator as Faker;

$factory->define(popular_tag::class, function (Faker $faker) {
    return [
        'name'=>$faker->title,
        'link'=>$faker->url,
        'is_active' => $faker->boolean,
        'is_draft'=>$faker->boolean,
        'soft_delete'=>$faker->boolean,

    ];
});
