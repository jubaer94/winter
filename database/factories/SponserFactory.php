<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\sponser;
use Faker\Generator as Faker;

$factory->define(sponser::class, function (Faker $faker) {
    return [
        'title'=>$faker->title,
        'picture'=>'1580297449__card6.jpg',
        'link'=>$faker->url,
        'promotional_message'=>$faker->url,
        'html_banner'=>$faker->text,
        'is_active' => $faker->boolean,
        'is_draft'=>$faker->boolean,
        'soft_delete'=>$faker->boolean,

    ];
});
