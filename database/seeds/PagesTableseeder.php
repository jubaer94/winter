<?php

use Illuminate\Database\Seeder;

class PagesTableseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\page::class,10)->create();
    }
}
