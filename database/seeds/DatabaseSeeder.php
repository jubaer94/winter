<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(UsersTableSeeder::class);
            $this->call(AdminTableSeeder::class);
            $this->call(BrandSeederTable::class);
            $this->call(BannersTableSeeder::class);
            $this->call(CartTableSeeder::class);
            $this->call(CategoriesTbaleseeder::class);
            $this->call(ContactTableSeeder::class);
            $this->call(LabelTableSeeder::class);
            $this->call(MapProductTagTableSeeder::class);
            $this->call(OrdersTableseedr::class);
            $this->call(PagesTableseeder::class);
            $this->call(PopularTagTableSeeder::class);
            $this->call(ProductlistTableSeeder::class);
            $this->call(SponserTableSeeder::class);
            $this->call(SubscribersTableSeeder::class);
            $this->call(TagTableSeeder::class);
            $this->call(TestimonialTableSeeder::class);

    }
}
