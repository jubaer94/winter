<?php

use Illuminate\Database\Seeder;

class ProductlistTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\productlist::class,50)->create();
    }
}
