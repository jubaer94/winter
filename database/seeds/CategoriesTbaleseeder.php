<?php

use Illuminate\Database\Seeder;

class CategoriesTbaleseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Categorie::class,10)->create();
    }
}
