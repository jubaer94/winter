<?php

use Illuminate\Database\Seeder;

class PopularTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\popular_tag::class,10)->create();
    }
}
