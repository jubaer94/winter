<?php

use Illuminate\Database\Seeder;

class MapProductTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\map_product_tag::class,50)->create();
    }
}
