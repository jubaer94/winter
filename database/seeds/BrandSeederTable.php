<?php

use Illuminate\Database\Seeder;

class BrandSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    factory(\App\Brand::class,20)->create();
    }
}
