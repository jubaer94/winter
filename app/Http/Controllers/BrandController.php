<?php

namespace App\Http\Controllers;

use App\Brand;
use App\productlist;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $brands=Brand::paginate(5);
        return view('dash-board.brands.index',compact('brands'));
//        $brands=Brand::find(1);
//        $branddata=$brands->productlist()->select('name')->get();
//        dd($branddata);



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        try {
            $branddata=$request->all();
            Brand::create($branddata);
            return redirect()->route('brand.index')->with('message','data inserted successfully');
        }
        catch (QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Brand $brand)
    {
        return view('dash-board.brands.delete',compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Brand $brand)
    {
       return  view('dash-board.brands.edit',compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, Brand $brand)
    {
        try {
            $updatedata=$request->all();
            $brand->update($updatedata);
        }
        catch (QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand  $brand
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Brand $brand)
    {
        try {
            $brand->delete();
            return redirect()->route('brand.index')->with('message','Data deleted successful');
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }

}
