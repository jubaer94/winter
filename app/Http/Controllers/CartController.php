<?php

namespace App\Http\Controllers;

use App\cart;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $carts=cart::paginate(5);
        return view('dash-board.carts.index',compact('carts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.carts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        try {


            if ($request->hasFile('picture')) {

                $cartdata = $request->all();
                $picturename = time() . '__' . $request->picture->getClientOriginalName();
                $request->picture->move('images/carts', $picturename);
                $cartdata['picture'] = $picturename;
                cart::create($cartdata);
                return redirect()->route('cart.create')->with('message', 'Data insertion successful');


            } else {
                $cartdata = $request->all();
                cart::create($cartdata);
                return redirect()->route('cart.create')->with('message', 'Data insertion successful');
            }
        } catch (QueryException $e) {
            {
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\cart  $cart
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(cart $cart)
    {
       return  view('dash-board.carts.show',compact('cart'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\cart  $cart
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(cart $cart)
    {
       return view('dash-board.carts.show',compact('cart'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\cart  $cart
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, cart $cart)
    {

        try {


            if ($request->hasFile('picture'))
            {
                $updatedata= $request->all();
                $picturename=time().'__'.$request->picture->getClientOriginalName();
                $request->picture->move('images/carts',$picturename);
                $updatedata['picture']=$picturename;

                $cart->update($updatedata);
                return redirect()->route("cart.show",[$cart])->with('message','Data updated successful');


            }
            else
            {
                $updatedata= $request->all();
                $cart->update($updatedata);
                return redirect()->route("cart.show",[$cart])->with('message','Data updated successful');
            }


        }
        catch (QueryException $e)
        {
            {
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\cart  $cart
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(cart $cart)
    {
        try {
            $cart->delete();
            return redirect()->route('cart.index')->with('message','Data deleted successful');
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }

}
