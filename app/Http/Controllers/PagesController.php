<?php

namespace App\Http\Controllers;

use App\page;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $pages=page::paginate(5);
        return view('dash-board.pages.index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $pagedata=$request->all();
            page::create($pagedata);
            return redirect()->route('page.index')->with('message','data inserted successfully');
        }
        catch (QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\page  $pages
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(page $pages)
    {
        return view('dash-board.pages.show',compact('pages'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\page  $pages
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(page $pages)
    {
        return view('dash-board.pages.edit',compact('pages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\page  $pages
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, page $pages)
    {
        try {
            $updatedata=$request->all();
            $pages->update($updatedata);
            return redirect()->route('page.show',[$pages])->with('message','data updated successfully');
        }
        catch (QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\page  $pages
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(page $pages)
    {
        try {
            $pages->delete();
            return redirect()->route('page.index')->with('message','data deleted successfully');
        }
        catch (\Exception $e)
        {
            return  redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

}
