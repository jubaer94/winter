<?php

namespace App\Http\Controllers;

use App\Billinfo;
use Illuminate\Http\Request;

class BillinfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('front-end.billinginfo');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $info=$request->all();
        Billinfo::create($info);
        return redirect()->route('billinginfo.index')->with('message','data inserted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Billinfo  $billinfo
     * @return \Illuminate\Http\Response
     */
    public function show(Billinfo $billinfo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Billinfo  $billinfo
     * @return \Illuminate\Http\Response
     */
    public function edit(Billinfo $billinfo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Billinfo  $billinfo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Billinfo $billinfo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Billinfo  $billinfo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Billinfo $billinfo)
    {
        //
    }
}
