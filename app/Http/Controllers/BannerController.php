<?php

namespace App\Http\Controllers;

use App\banner;
use App\Brand;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $banners=banner::paginate(5);

        return view('dash-board.banners.index',compact('banners'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        //
        return view('dash-board.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        try {

            if ($request->hasFile('picture'))
            {
                $bannerdata= $request->all();
                $picturename=time().'__'.$request->picture->getClientOriginalName();
                 $request->picture->move('images/Banners',$picturename);
                 $bannerdata['picture']=$picturename;
               banner::create($bannerdata);
                return redirect()->route('banners.create')->with('message','Data insertion successful');

            }
            else {
                $bannerdata= $request->all();
                banner::create($bannerdata);
                return redirect()->route('banners.create')->with('message','Data insertion successful');
            }


        }
        catch (QueryException $e)
        {
            {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
        }

//        try {
//            banner::create($bannerdata);
//            return redirect()->route('banners.create')->with('message','Data insertion successful');
//        }
//        catch (QueryException $e)
//        {
//            return redirect()->back()->withInput()->withErrors($e->getMessage());
//        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\banner  $banner
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(banner $banner)
    {
        return view('dash-board.banners.show',compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\banner  $banner
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(banner $banner)
    {
          return view('dash-board.banners.edit',compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\banner  $banner
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, banner $banner)
    {

        try {


            if ($request->hasFile('picture'))
            {
                $updatedata= $request->all();
                $picturename=time().'__'.$request->picture->getClientOriginalName();
                $request->picture->move('images/Banners',$picturename);
                $updatedata['picture']=$picturename;

                $banner->update($updatedata);
                return redirect()->route("banners.show",[$banner])->with('message','Data updated successful');


            }
            else
            {
                $updatedata= $request->all();
                $banner->update($updatedata);
                return redirect()->route("banners.show",[$banner])->with('message','Data updated successful');
            }


        }
        catch (QueryException $e)
        {
            {
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }
        }

//        try {
//            $updatedata=$request->all();
//            $banner->update($updatedata);
//            return redirect()->route("banners.show",[$banner])->with('message','Data updated successful');
//        }
//        catch (QueryException $e)
//        {
//            return redirect()->back()->withInput()->withErrors($e->getMessage());
//        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\banner  $banner
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(banner $banner)
    {
        try {
            $banner->delete();
            return redirect()->route('banners.index')->with('message','Data deleted successful');
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }

}
