<?php

namespace App\Http\Controllers;

use App\subscriber;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class SubscriberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $subscribers=subscriber::paginate(10);
        return view('dash-board.subscribers.index',compact('subscribers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.subscribers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $subscribersdata=$request->all();
        try {
            subscriber::create($subscribersdata);
            return redirect()->route('subscriber.index')->with('message','data inserted successfully');
        }
        catch (QueryException $e)
        {
            return redirect()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\subscriber  $subscriber
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(subscriber $subscriber)
    {
       return view('dash-board.subscribers.show',compact('subscriber'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\subscriber  $subscriber
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(subscriber $subscriber)
    {
        return view('dash-board.subscribers.edit',compact('subscriber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\subscriber  $subscriber
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, subscriber $subscriber)
    {
        try {
            $updatedata=$request->all();
            $subscriber->update($updatedata);
            return redirect()->route('subscriber.show',[$subscriber])->with('message','data updated successfully');
        }
        catch (QueryException $e)
        {
            return redirect()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\subscriber  $subscriber
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(subscriber $subscriber)
    {
        try {
            $subscriber->delete();
            return redirect()->route('subscriber.index')->with('message','data deleted successfully');
        }
        catch (\Exception $e)
        {
            return redirect()->withInput()->withErrors($e->getMessage());
        }
    }
}
