<?php

namespace App\Http\Controllers;

use App\admin;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $admin=admin::paginate(5);
        return view('dash-board.admins.index',compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $admindata= $request->all();

        try {
            admin::create($admindata);
            return redirect()->route('admin.create')->with('message','Data insertion successful');
        }
        catch (QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\admin  $admin
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(admin $admin)
    {
        return view('dash-board.admins.show',compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\admin  $admin
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(admin $admin)
    {
        return  view('dash-board.admins.edit',compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\admin  $admin
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, admin $admin)
    {
        try {
            $updatedata=$request->all();
            $admin->update($updatedata);
            return redirect()->route("admin.show",[$admin])->with('message','Data updated successful');
        }
        catch (QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\admin  $admin
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(admin $admin)
    {
        try {
            $admin->delete();
            return redirect()->route('admin.index')->with('message','Data deleted successful');
        }
        catch (\Exception $e) {
        }
        return redirect()->back()->withInput()->withErrors($e->getMessage());
    }

}
