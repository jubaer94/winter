<?php

namespace App\Http\Controllers;

use App\testimonial;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class TestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $testimonials=testimonial::paginate(5);
        return view('dash-board.testimonial.index',compact('testimonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.testimonial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {


            if ($request->hasFile('picture')) {
                $testimonialdata = $request->all();
                $picturename = time() . '__' . $request->picture->getClientOriginalName();
                $request->picture->move('images/testimonials', $picturename);
                $testimonialdata['picture'] = $picturename;
                testimonial::create($testimonialdata);
                return redirect()->route('testimonial.index');

            } else {
                $testimonialdata = $request->all();
                testimonial::create($testimonialdata);
                return redirect()->route('testimonial.index');

            }
        }
        catch (QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\testimonial  $testimonial
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(testimonial $testimonial)
    {
        return view('dash-board.testimonial.show',compact('testimonial'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\testimonial  $testimonial
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(testimonial $testimonial)
    {
        return view('dash-board.testimonial.edit',compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\testimonial  $testimonial
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, testimonial $testimonial)
    {
        try {


            if ($request->hasFile('picture')) {
                $updatedata = $request->all();
                $picturename = time() . '__' . $request->picture->getClientOriginalName();
                $request->picture->move('images/testimonials', $picturename);
                $updatedata['picture'] = $picturename;
                $testimonial->update($updatedata);
                return redirect()->route('testimonial.show', [$testimonial])->with('message', 'data updated successfully');
            }
            else
            {
                $updatedata=$request->all();
                $testimonial->update($updatedata);
                return redirect()->route('testimonial.show', [$testimonial])->with('message', 'data updated successfully');
            }
        }
        catch (QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\testimonial  $testimonial
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(testimonial $testimonial)
    {
        try {
            $testimonial->delete();
            return redirect()->route('testimonial.index')->with('message','Data deleted successful');
        }
        catch (\Exception $e) {
        }
        return redirect()->back()->withInput()->withErrors($e->getMessage());
    }

}
