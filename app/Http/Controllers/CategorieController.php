<?php

namespace App\Http\Controllers;

use App\Categorie;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\RedirectResponse
     */

    public function __construct()
    {
        $this->middleware('auth');
        return redirect()->back();
    }


    public function index()
    {
        $categories=Categorie::paginate(5);
        return view('dash-board.categories.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
//        try {
//            $categoriedata=$request->all();
//            Categorie::create($categoriedata);
//            return redirect()->route('categorie.index')->with('message','data inserted successful');
//        }
//        catch (QueryException $e)
//        {
//            return redirect()->back()->withInput()->withErrors($e->getMessage());
//        }
        try {
            if ($request->hasFile('picture')) {
                $categoriedata = $request->all();
                $picturename = time() . '__' . $request->picture->getClientOriginalName();
                $request->picture->move('images/categories', $picturename);
                $categoriedata['picture'] = $picturename;
                Categorie::create($categoriedata);
                return redirect()->route('categorie.index')->with('message', 'data inserted successfully');
            } else {
                $categoriedata = $request->all();
                Categorie::create($categoriedata);
                return redirect()->route('categorie.index')->with('message', 'data inserted successful');
            }
        } catch (QueryException $e) {
            {
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Categorie  $categorie
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Categorie $categorie)
    {

        return view('dash-board.categories.show',compact('categorie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Categorie  $categorie
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Categorie $categorie)
    {
        return view('dash-board.categories.edit',compact('categorie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Categorie  $categorie
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, Categorie $categorie)
    {
        try {


            if ($request->hasFile('picture'))
            {
                $updatedata= $request->all();
                $picturename=time().'__'.$request->picture->getClientOriginalName();
                $request->picture->move('images/categories',$picturename);
                $updatedata['picture']=$picturename;

                $categorie->update($updatedata);
                return redirect()->route("categorie.show",[$categorie])->with('message','Data updated successful');


            }
            else
            {
                $updatedata= $request->all();
               $categorie->update($updatedata);
                return redirect()->route("categorie.show",[$categorie])->with('message','Data updated successful');
            }


        }
        catch (QueryException $e)
        {
            {
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Categorie  $categorie
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Categorie $categorie)
    {
        try {
            $categorie->delete();
            return redirect()->route('categorie.index')->with('message','item deleted successfully');
        }
        catch (\Exception $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
}
