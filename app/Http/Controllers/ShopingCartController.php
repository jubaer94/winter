<?php

namespace App\Http\Controllers;
use App\AddToCart;
use App\productlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ShopingCartController extends Controller
{
    public function addtocart(Request $request,$id)
    {
        $product=productlist::find($id);
        $oldcart=Session::has('cart') ? Session::get('cart'):null ;
        $cart= new AddToCart($oldcart);
        $cart->additem($product,$product->id);
        $request->session()->put('cart',$cart);

        return redirect()->route('category.index');

    }
}
