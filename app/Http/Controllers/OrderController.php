<?php

namespace App\Http\Controllers;

use App\order;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
       $orders=order::paginate(5);
       return view('dash-board.orders.index',compact('orders'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        try {
            $orderdata=$request->all();
            order::create($orderdata);
            return redirect()->route('order.index')->with('message','data inserted successfully');
        }
        catch (QueryException $e)
        {
            return redirect()->back()->onlyInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(order $order)
    {
        return  view('dash-board.orders.show',compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(order $order)
    {
        return  view('dash-board.orders.edit',compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\order  $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, order $order)
    {
        try {
            $updatedata=$request->all();
            $order->update($updatedata);
            return redirect()->route('order.show',[$order])->with('data updated successfully');
        }
        catch (QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\order  $order
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(order $order)
    {
        try {
            $order->delete();
            return redirect()->route('order.index')->with('message','data deleted successfully');
        }
        catch (\Exception $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }
}
