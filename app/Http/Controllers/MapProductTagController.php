<?php

namespace App\Http\Controllers;

use App\map_product_tag;
use Illuminate\Http\Request;

class MapProductTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $map_product_tags=map_product_tag::paginate(5);
        return view('dash-board.map_product_tags.index',compact('map_product_tags'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.map_product_tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $map_product_tagdata=$request->all();
            map_product_tag::create($map_product_tagdata);
            return redirect()->route('map_product_tags.index')->with('message','data inserted successfully');
        }
        catch (QueryException $e)
        {
            return redirect()->back()->onlyInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\map_product_tag  $map_product_tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(map_product_tag $map_product_tag)
    {
        return  view('dash-board.map_product_tags.show',compact('map_product_tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\map_product_tag  $map_product_tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(map_product_tag $map_product_tag)
    {
        return  view('dash-board.map_product_tags.edit',compact('map_product_tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\map_product_tag  $map_product_tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, map_product_tag $map_product_tag)
    {
        try {
            $updatedata=$request->all();
            $map_product_tag->update($updatedata);
            return redirect()->route('map_product_tags.show',[$map_product_tag])->with('data updated successfully');
        }
        catch (QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\map_product_tag  $map_product_tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(map_product_tag $map_product_tag)
    {
        try {
            $map_product_tag->delete();
            return redirect()->route('map_product_tags.index')->with('message','data deleted successfully');
        }
        catch (\Exception $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }

}
