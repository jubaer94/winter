<?php

namespace App\Http\Controllers;

use App\popular_tag;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class PopularTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
      $populartags=popular_tag::paginate(10);
      return view('dash-board.popular_tag.index',compact('populartags')) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.popular_tag.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $data=$request->all();
            popular_tag::create($data);
            return redirect()->route('populartag.index')->with('message','data inserted successfully');
        }
        catch (QueryException $e)
        {
            return redirect()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\popular_tag  $popular_tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(popular_tag $popular_tag)
    {
        return view('dash-board.popular_tag.show',compact('popular_tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\popular_tag  $popular_tag
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(popular_tag $popular_tag)
    {
     return view('dash-board.popular_tag.edit',compact('popular_tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\popular_tag  $popular_tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, popular_tag $popular_tag)
    {
        try {
            $updatedata=$request->all();
            $popular_tag->update($updatedata);
            return redirect()->route('populartag.show',[$popular_tag])->with('message','data updated successfully');

        }
        catch (QueryException $e)
        {
            return redirect()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\popular_tag  $popular_tag
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(popular_tag $popular_tag)
    {
        try {
            $popular_tag->delete();
            return redirect()->route('populartag.index')->with('message','datta deleted successfully');
        }
        catch (\Exception $e)
        {
            return redirect()->withInput()->withErrors($e->getMessage());
        }
    }
}
