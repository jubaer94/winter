<?php

namespace App\Http\Controllers;

use App\producte;
use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;

class ProducteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $product= (Cart::content());
        dd($product);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.producte.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo("hello world");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\producte  $producte
     * @return \Illuminate\Http\Response
     */
    public function show(producte $producte)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\producte  $producte
     * @return \Illuminate\Http\Response
     */
    public function edit(producte $producte)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\producte  $producte
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, producte $producte)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\producte  $producte
     * @return \Illuminate\Http\Response
     */
    public function destroy(producte $producte)
    {
        //
    }
}
