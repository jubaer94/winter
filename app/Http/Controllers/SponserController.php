<?php

namespace App\Http\Controllers;

use App\sponser;
use Illuminate\Http\Request;

class SponserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $sponsers=sponser::paginate(5);
        return view('dash-board.sponsers.index',compact('sponsers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
       return view('dash-board.sponsers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {

            if ($request->hasFile('picture'))
            {
                $sponserdata= $request->all();
                $picturename=time().'__'.$request->picture->getClientOriginalName();
                $request->picture->move('images/sponsers',$picturename);
                $sponserdata['picture']=$picturename;
                sponser::create( $sponserdata);
                return redirect()->route('sponser.create')->with('message','Data insertion successful');

            }
            else {
                $sponserdata= $request->all();
                sponser::create( $sponserdata);
                return redirect()->route('sponser.create')->with('message','Data insertion successful');
            }


        }
        catch (QueryException $e) {
            {
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sponser  $sponser
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(sponser $sponser)
    {
        return view('dash-board.sponsers.show',compact('sponser'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sponser  $sponser
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(sponser $sponser)
    {
      return view('dash-board.sponsers.edit',compact('sponser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sponser  $sponser
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, sponser $sponser)
    {
        try {


            if ($request->hasFile('picture'))
            {
                $updatedata= $request->all();
                $picturename=time().'__'.$request->picture->getClientOriginalName();
                $request->picture->move('images/sponsers',$picturename);
                $updatedata['picture']=$picturename;

                $sponser->update($updatedata);
                return redirect()->route("sponser.show",[$sponser])->with('message','Data updated successful');


            }
            else
            {
                $updatedata= $request->all();
                $sponser->update($updatedata);
                return redirect()->route("sponser.show",[$sponser])->with('message','Data updated successful');
            }


        }
        catch (QueryException $e)
        {
            {
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sponser  $sponser
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(sponser $sponser)
    {
        try {
            $sponser->delete();
            return redirect()->route('sponser.index')->with('message','Data deleted successful');
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }

}
