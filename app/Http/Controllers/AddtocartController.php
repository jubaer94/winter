<?php

namespace App\Http\Controllers;

use Gloudemans\Shoppingcart\Facades\Cart;

use App\productlist;
use Illuminate\Http\Request;

class AddtocartController extends Controller
{
    public function Add(Request $request,$id)
    {
        $product=productlist::find($id);



       $cartItem=Cart::add(['id' => $product->id, 'name' => $product->name, 'qty' => $request->qty, 'price' => $product->mrp, 'weight' => 0]);
        $cartItem->associate(productlist::class);

        return redirect()->route('carts');
    }
    public function viewcart()
    {

       return view('front-end.cart');
    }
    public function delete($id)
    {
        Cart::remove($id);
        return redirect()->back();

    }
    public function incr($id,$qty)
    {
    Cart::update($id, $qty +1);
    return redirect()->back();
    }
    public function decr($id,$qty)
    {
        Cart::update($id, $qty -1);
        return redirect()->back();
    }

}
