<?php

namespace App\Http\Controllers;

use App\label;
use Illuminate\Http\Request;

class LabelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $labels=label::paginate(5);
        return view('dash-board.labels.index',compact('labels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return  view('dash-board.labels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {

        try {

            if ($request->hasFile('picture'))
            {
                $labeldata= $request->all();
                $picturename=time().'__'.$request->picture->getClientOriginalName();
                $request->picture->move('images/labels',$picturename);
                $labeldata['picture']=$picturename;
                label::create( $labeldata);
                return redirect()->route('label.create')->with('message','Data insertion successful');

            }
            else {
                $labeldata= $request->all();
                label::create( $labeldata);
                return redirect()->route('label.create')->with('message','Data insertion successful');
            }


        }
        catch (QueryException $e)
        {
            {
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\label  $label
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(label $label)
    {
      return view('dash-board.labels.show',compact('label'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\label  $label
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(label $label)
    {
        return view('dash-board.labels.edit',compact('label'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\label  $label
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, label $label)
    {
        try {


            if ($request->hasFile('picture'))
            {
                $updatedata= $request->all();
                $picturename=time().'__'.$request->picture->getClientOriginalName();
                $request->picture->move('images/labels',$picturename);
                $updatedata['picture']=$picturename;

                $label->update($updatedata);
                return redirect()->route("label.show",[$label])->with('message','Data updated successful');


            }
            else
            {
                $updatedata= $request->all();
                $label->update($updatedata);
                return redirect()->route("label.show",[$label])->with('message','Data updated successful');
            }


        }
        catch (QueryException $e)
        {
            {
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\label  $label
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(label $label)
    {
        try {
            $label->delete();
            return redirect()->route('label.index')->with('message','Data deleted successful');
        }
        catch (\Exception $e) {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }

    }

}
