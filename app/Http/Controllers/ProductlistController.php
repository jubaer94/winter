<?php

namespace App\Http\Controllers;

use App\Brand;
use App\product;
use App\productlist;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ProductlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $productlist=productlist::paginate(5);

        return view('dash-board.productlist.index',compact('productlist'));
//        $products = productlist::with(['Brand' => function ($query) {
//            $query->select('title', 'link')->get();
//        }])->get();
//        $products=productlist::with('Brand:id,title')->get();
//        dd($products->toArray());
//
//        return view('dash-board.brands.delete',compact('products'));
//        dd();


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('dash-board.productlist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {

            if ($request->hasFile('picture'))
            {
                $productlistdata= $request->all();
                $picturename=time().'__'.$request->picture->getClientOriginalName();
                $request->picture->move('images/productlist',$picturename);
                $productlistdata['picture']=$picturename;
                productlist::create($productlistdata);
                return redirect()->route('productlist.create')->with('message','Data insertion successful');

            }
            else {
                $productlistdata= $request->all();
                productlist::create($productlistdata);
                return redirect()->route('productlist.create')->with('message','Data insertion successful');
            }


        }
        catch (QueryException $e)
        {

                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }

//        $productlistdata= $request->all();
//
//        try {
//            productlist::create($productlistdata);
//            return redirect()->route('productlist.create')->with('message','Data insertion successful');
//        }
//        catch (QueryException $e)
//        {
//            return redirect()->back()->withInput()->withErrors($e->getMessage());
//        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\productlist  $productlist
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(productlist $productlist)
    {
           return view('dash-board.productlist.show',compact('productlist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\productlist  $productlist
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(productlist $productlist)
    {
        return view('dash-board.productlist.edit',compact('productlist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\productlist  $productlist
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, productlist $productlist)
    {


        try {


            if ($request->hasFile('picture')) {
                $updatedata = $request->all();
                $picturename = time() . '__' . $request->picture->getClientOriginalName();
                $request->picture->move('images/productlist', $picturename);
                $updatedata['picture'] = $picturename;

                $productlist->update($updatedata);
                return redirect()->route("productlist.show", [$productlist])->with('message', 'Data updated successful');


            } else {
                $updatedata = $request->all();
                $productlist->update($updatedata);
                return redirect()->route("productlist.show", [$productlist])->with('message', 'Data updated successful');
            }

        }
//        try {
//            $updatedata=$request->all();
//            $productlist->update($updatedata);
//            return redirect()->route("productlist.show",[$productlist])->with('message','Data updated successful');
//        }
//        catch (QueryException $e)
//        {
//            return redirect()->back()->withInput()->withErrors($e->getMessage());
//        }
        catch (QueryException $e)
        {
            {
                return redirect()->back()->withInput()->withErrors($e->getMessage());
            }
        }





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\productlist  $productlist
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(productlist $productlist)
    {

        try {
             $productlist->delete();
            return redirect()->route('productlist.index')->with('message','Data deleted successful');
        }
         catch (\Exception $e) {
        }
        return redirect()->back()->withInput()->withErrors($e->getMessage());
    }
}
