<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\This;

class productlist extends Model
{
    //
    protected $fillable = ['name', 'title','brand_id','label_id','picture','short_description','description','total_sales','is_new',
        'cost','mrp','special_price','is_draft','is_active','soft_delete','category_id'];
    public  function brand()
    {
        return  $this->belongsTo(Brand::class);
    }
    public function tag()
    {
        return $this->belongsToMany(tag::class, 'map_product_tags', 'product_id', 'tag_id');
    }
    public function label()
    {
        return $this->belongsTo(label::class);
    }
    public function cart()
    {
        return $this->hasMany(cart::class,'product_id');
    }
    public function order()
    {
        return $this->hasMany(order::class,'product_id');
    }
    public function categorie()
    {
        return  $this->belongsTo(Categorie::class);
    }
}
