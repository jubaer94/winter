<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tag extends Model
{
    protected $fillable=['title'];
    public function productlist()
    {
        return $this->belongsToMany(productlist::class, 'map_product_tags', 'tag_id', 'product_id');
    }
}
