<?php


namespace App;


class AddToCart
{
    public $items=null;
    public $totalqty=0;
    public $totalPrice=0;

    public function __construct($oldcart)
    {
        if ($oldcart)
        {
            $this->items=$oldcart->items;
            $this->totalPrice=$oldcart->totalPrice;
            $this->totalqty=$oldcart->totalqty;
        }


    }
    public  function additem($item,$id)
    {
        $storeditem=['qty'=>0,'price'=>$item->mrp,'item'=>$item];
        if ($this->items) {


            if (array_key_exists($id,$this->items))
            {
                $storeditem=$this->items[$id];
            }

            }
        $storeditem['qty']++;
        $storeditem['price']=$item->mrp*$storeditem['qty'];
        $this->items[$id]=$storeditem;
        $this->totalqty++;
        $this->totalPrice+=$item->mrp;
    }

}
