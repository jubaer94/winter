<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cart extends Model
{
   protected $fillable=['sid','product_id','picture','product_title','qty','unite_price','total_price'];
   public function productlist()
   {
       return $this->belongsTo(productlist::class);
   }
}
