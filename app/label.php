<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class label extends Model
{
    protected $fillable=['picture','title'];

    public function productlist()
    {
        return $this->hasMany(productlist::class,'label_id');
    }
}
