<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billinfo extends Model
{
    protected $fillable=['user_id','first_name','last_name','company_name','address','district','country','zipcode'];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
