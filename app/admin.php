<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    protected $fillable=['name','email','phone','password','is_draft','soft_delete'];
}
