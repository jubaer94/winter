<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    protected $fillable=['name','link','picture','soft_delete','is_draft'];
    public function productlist()
    {
        return $this->hasMany(productlist::class,'Categorie_id');

    }
}

