<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $fillable=['product_id','qty'];
    public function productlist()
    {
        $this->belongsTo(productlist::class);
    }
}
