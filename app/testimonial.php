<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class testimonial extends Model
{
    protected $fillable=['name','picture','body','designation','is_draft','is_active','soft_delete',];
}
