<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class map_product_tag extends Model
{
    protected $fillable=['product_id','tag_id'];
}
