<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

use Illuminate\Support\Facades\Route;
//Route::get('/',function (){
//    return view('front-end.index');
//
//})->name('home');

Route::get('/about', function () {
    return view('front-end.about');
})->name('about');
Route::get('/blog', function () {
    return view('front-end.blog');
})->name('blog');
//Route::get('/carts', function () {
//    return view('front-end.cart');
//})->name('carts');

Route::get('carts','AddtocartController@viewcart')->name('carts');
Route::get('/category', function () {
    return view('front-end.category');
})->name('category');
//Route::get('/checkout', function () {
//    return view('front-end.checkout');
//})->name('checkout');
Route::get('/confirmation', function () {
    return view('front-end.confirmation');
})->name('confirmation');
Route::get('/checkout_login', function () {
    return view('front-end.login');
})->name('checkout_login');
Route::get('/single-blog', function () {
    return view('front-end.single-blog');
})->name('single-blog');
Route::get('/single-product', function () {
    return view('front-end.single-product');
})->name('single-product');
Route::get('/elements', function () {
    return view('front-end.elements');
})->name('elements');
Route::get('/tracking', function () {
    return view('front-end.tracking');
})->name('tracking');
//Route::get('/', function () {
//    return view('front-end.index');
//})->name('profile');
Route::get('/category', function () {
    return view('front-end.category');
})->name('category');
Route::get('/profile', function () {
    return view('back-end.admin');
})->name('profile');

Route::get('/contacts', function () {
    return view('front-end.contact');
})->name('contacts');
Route::resource('brand','BrandController');
Route::resource('productlist','ProductlistController');
Route::resource('producte','producteController');
Route::resource('banners','BannerController');
Route::resource('cart','CartController');
Route::resource('categorie','CategorieController');
Route::resource('label','LabelController');
Route::resource('contact','ContactController');
Route::resource('map_product_tags','MapProductTagController');
Route::resource('order','OrderController');
Route::resource('page','PagesController');
Route::resource('populartag','PopularTagController');
Route::resource('slider','SliderController');
Route::resource('sponser','SponserController');
Route::resource('subscriber','SubscriberController');
Route::resource('tag','TagController');
Route::resource('testimonial','TestimonialController');
Route::resource('admin','AdminController');
Route::get('/winter','TestController@index')->name('index');
Route::resource('winter','WinterHomeController');
Route::resource('category','WinterCategoryController');
Route::resource('singlebrand','SingleBrandController');
Route::resource('singleproduct','SingleProductController');
Route::resource('checkout','checkoutController');
Route::resource('billinginfo','BillinfoController');
Route::post('paycheckout','PaycheckoutController@pay')->name('paycheckout');
Route::get('payindex','PaycheckoutController@index')->name('payindex');

//Route::get('addtocart/{id}','ShopingCartController@addtocart')->name('addtocart');
Route::post('Addtocart/{id}','AddtocartController@Add');
Route::get('/carts/delete/{id}','AddtocartController@delete')->name('carts.delete');
Route::get('carts/incr/{id}/{qty}','AddtocartController@incr')->name('carts.incr');
Route::get('carts/decr/{id}/{qty}','AddtocartController@decr')->name('carts.decr');
Route::resource('content','ProducteController');










Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
