@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')



@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        @if($errors->any())
                            <div class="alert alert-danger">

                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach


                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                    @endif
                    <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">labels</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            {!! Form::open(array('url' => "label/$label->id",'method' => 'put','enctype'=>"multipart/form-data")) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    {{ Form::label("picture", null, ['class' => 'control-label']) }}
                                    <br>
                                    <td><img src="{{asset("images/labels/$label->picture")}}" alt="HTML5 Icon" style="width:150px;height:150px" ></td>
                                    <br>

                                    {{Form::file("picture",null,["class" => "form-control",]) }}


                                </div>


                                </div>
                                <div class="form-group">

                                    {{ Form::label("title", null, ['class' => 'control-label']) }}


                                    {{Form::text('title', old('title')? old('title'):(!empty($label)?$label->title:null),["class" => "form-control",
                "placeholder" => "title ","id"=>"title"])}}

                                </div>

                                <!-- /.card-body -->

                                <div class="card-footer">
                                    {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                    {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- /.card -->

