@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')



@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        @if($errors->any())
                            <div class="alert alert-danger">

                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach


                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                    @endif
                    <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">pages</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            {!! Form::open(array('url' => "page/$pages->id",'method' => 'put','enctype'=>"multipart/form-data")) !!}
                            <div class="card-body">



                            </div>
                            <div class="form-group">

                                {{ Form::label("description", null, ['class' => 'control-label']) }}


                                {{Form::text('description', old('description')? old('description'):(!empty($pages)?$pages->description:null),["class" => "form-control",
            "placeholder" => "description ","id"=>"description"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("link", null, ['class' => 'control-label']) }}


                                {{Form::text('link', old('link')? old('link'):(!empty($pages)?$pages->link:null),["class" => "form-control",
            "placeholder" => "link ","id"=>"link"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("title", null, ['class' => 'control-label']) }}


                                {{Form::text('title', old('title')? old('title'):(!empty($pages)?$pages->title:null),["class" => "form-control",
            "placeholder" => "title ","id"=>"title"])}}

                            </div>






                            <!-- /.card-body -->

                            <div class="card-footer">
                                {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.card -->

