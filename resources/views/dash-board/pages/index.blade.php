@extends('layouts.backendmaster')


@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')


{{--<div class="col-md-6">--}}

{{--    <table class="table table-hover table-striped">--}}


{{--<tr>--}}
{{--    <th>page name</th>--}}
{{--    <th>page title</th>--}}
{{--    <th>actions</th>--}}
{{--</tr>--}}
{{--        @foreach( $page as $key => $page)--}}
{{--    <tr>--}}
{{--        <td>{{$page->firstItem()+ $key}}</td>--}}
{{--        <td>{{$page->name}}</td>--}}
{{--        <td>{{$page->title}}</td>--}}
{{--        <td>--}}
{{--            <a href="{{url("banners/$page->id")}}">show</a>--}}
{{--            <a href="{{url("banners/$page->id/edit")}}">edit</a>--}}
{{--            edit | delete | show--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--            @endforeach--}}
{{--    </table>--}}
{{--    {{$page->links()}}--}}
{{--</div>--}}
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('page/create')}}">CREATE</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable of banners</h3>
                        </div>
                        <div class="col-md-6">
                            @if($errors->any())
                                <div class="alert alert-danger">

                                    @foreach($errors->all() as $error)
                                        {{$error}}
                                    @endforeach


                                </div>
                            @endif

                            @if(session('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                        @endif
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th>description</th>
                                        <th>link</th>
                                        <th>title</th>
                                        <th>actions</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($pages as $key => $page)
                                        <tr>
                                            <td>{{$pages->firstItem()+ $key}}</td>
                                            <td>{{$page->description}}</td>
                                            <td>{{$page->link}}</td>
                                            <td>{{$page->title}}</td>
                                            <td>
                                                <a href="{{url("page/$page->id")}}" class="btn" type="submit">show</a>
                                                <a href="{{url("page/$page->id/edit")}} " class="btn" type="submit">edit</a>


                                                {!! Form::open(array('url' => "page/$page->id",'method' => 'delete' , 'class'=>'pull-right')) !!}
                                                {{Form::submit('delete', ['class' =>'btn btn-danger'])}}

                                                {!! Form::close() !!}


                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>

                                </table>
                                {{$pages->links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        @endsection;

