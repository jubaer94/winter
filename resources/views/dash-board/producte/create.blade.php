@extends('layouts.backendmaster')

@include('elements.back-end.navbar')




@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    @if($errors->any())
                        <div class="alert alert-danger">

                            @foreach($errors->all() as $error)
                                {{$error}}
                            @endforeach


                        </div>
                    @endif

                    @if(session('message'))
                        <div class="alert alert-success">
                            {{session('message')}}
                        </div>
                @endif
                <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">product list</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        {!! Form::open(array('url' => 'producte','method' => 'post')) !!}
                        <div class="card-body">
                            <div class="form-group">
                                {{ Form::label("name", null, ['class' => 'control-label']) }}

                                {{-- <label for="name">name</label>--}}
                                {{--                                    <input type="text" class="form-control" id="name" placeholder="enter your name">--}}
                                {{Form::text('name',null,["class" => "form-control",
            "placeholder" => "name","id"=>"name"])}}


                            </div>
                            <div class="form-group">
                                {{ Form::label("slug", null, ['class' => 'control-label']) }}

                                {{-- <label for="name">name</label>--}}
                                {{--                                    <input type="text" class="form-control" id="name" placeholder="enter your name">--}}
                                {{Form::text('slug',null,["class" => "form-control",
            "placeholder" => "slug","id"=>"slug"])}}


                            </div>
                            <div class="form-group">
                                {{ Form::label("barcode", null, ['class' => 'control-label']) }}

                                {{-- <label for="name">name</label>--}}
                                {{--                                    <input type="text" class="form-control" id="name" placeholder="enter your name">--}}
                                {{Form::text('sku',null,["class" => "form-control",
            "placeholder" => "sku","id"=>"sku"])}}


                            </div>
                            <div class="form-group">
                                {{ Form::label("barcode", null, ['class' => 'control-label']) }}

                                {{-- <label for="name">name</label>--}}
                                {{--                                    <input type="text" class="form-control" id="name" placeholder="enter your name">--}}
                                {{Form::text('barcode',null,["class" => "form-control",
            "placeholder" => "name","id"=>"barcode"])}}


                            </div>
                            <div class="form-group">
                                {{ Form::label("description", null, ['class' => 'control-label']) }}

                                {{-- <label for="name">name</label>--}}
                                {{--                                    <input type="text" class="form-control" id="name" placeholder="enter your name">--}}
                                {{Form::textarea('description',null,["class" => "form-control",
            "placeholder" => "description","id"=>"name"])}}


                            </div>
{{--                            <div class="form-group">--}}
{{--                                {{ Form::label("status", null, ['class' => 'control-label']) }}--}}

{{--                                --}}{{-- <label for="name">name</label>--}}
{{--                                --}}{{--                                    <input type="text" class="form-control" id="name" placeholder="enter your name">--}}
{{--                                {{Form::number('status',null,["class" => "form-control",--}}
{{--            "placeholder" => "status(the in put should be integer)","id"=>"status"])}}--}}


                            </div>
                            <div class="form-group">
                                {{ Form::label("status", null, ['class' => 'control-label']) }}

                                {{ Form::label("yes", null, ['class' => 'control-label']) }}
                                {{Form::radio("status","yes",true,["class"=>"form-group"])}}
                                {{ Form::label("no", null, ['class' => 'control-label']) }}
                                {{Form::radio("status","no",false,["class"=>"form-group"])}}

                            </div>

                            <div class="form-group">
                                {{ Form::label("in_stock", null, ['class' => 'control-label']) }}

                                {{-- <label for="name">name</label>--}}
                                {{--                                    <input type="text" class="form-control" id="name" placeholder="enter your name">--}}
                                {{Form::number('in_stock',null,["class" => "form-control",
            "placeholder" => "in_stock(the in put should be integer)","id"=>"in_stock"])}}


                            </div>

                            <div class="form-group">
                                {{--                                    <label for="title">title</label>--}}
                                {{--                                    <input type="text" class="form-control" id="title" placeholder="title">--}}
                                {{ Form::label("title", null, ['class' => 'control-label']) }}

                                {{-- <label for="name">name</label>--}}
                                {{--                                    <input type="text" class="form-control" id="name" placeholder="enter your name">--}}
                                {{Form::text('title',null,["class" => "form-control",
            "placeholder" => "title","id"=>"title"])}}

                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card -->





    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{asset('back-end/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('back-end/plugins/bootstrap/js/bootstrap.bundle.js')}}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{asset('back-end/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });

@endsection

