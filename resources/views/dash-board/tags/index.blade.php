@extends('layouts.backendmaster')


@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')


{{--<div class="col-md-6">--}}

{{--    <table class="table table-hover table-striped">--}}


{{--<tr>--}}
{{--    <th>brand name</th>--}}
{{--    <th>brand title</th>--}}
{{--    <th>actions</th>--}}
{{--</tr>--}}
{{--        @foreach( $tags as $key => $tag)--}}
{{--    <tr>--}}
{{--        <td>{{$tags->firstItem()+ $key}}</td>--}}
{{--        <td>{{$tag->name}}</td>--}}
{{--        <td>{{$tag->title}}</td>--}}
{{--        <td>--}}
{{--            <a href="{{url("banners/$tag->id")}}">show</a>--}}
{{--            <a href="{{url("banners/$tag->id/edit")}}">edit</a>--}}
{{--            edit | delete | show--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--            @endforeach--}}
{{--    </table>--}}
{{--    {{$tags->links()}}--}}
{{--</div>--}}
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('tag/create')}}">CREATE</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable of tags</h3>
                        </div>
                        <div class="col-md-6">
                            @if($errors->any())
                                <div class="alert alert-danger">

                                    @foreach($errors->all() as $error)
                                        {{$error}}
                                    @endforeach


                                </div>
                            @endif

                            @if(session('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                        @endif
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-btaged table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th>tag id</th>
                                        <th>title</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($tags as $key => $tag)
                                        <tr>
                                            <td>{{$tags->firstItem()+ $key}}</td>

                                            <td>{{$tag->id}}</td>
                                            <td>{{$tag->title}}</td>


                                            <td>
                                                <a href="{{url("tag/$tag->id")}}" class="btn" type="submit">show</a>
                                                <a href="{{url("tag/$tag->id/edit")}} " class="btn" type="submit">edit</a>


                                                {!! Form::open(array('url' => "tag/$tag->id",'method' => 'delete' , 'class'=>'pull-right')) !!}
                                                {{Form::submit('delete', ['class' =>'btn btn-danger'])}}

                                                {!! Form::close() !!}


                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>
                                    <tfoot>

                                    </tfoot>
                                </table>
                                {{$tags->links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.card -->
        @endsection;

