@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')



@section('content')


    <!-- Main content -->
    <div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <!-- left column -->
                <div class="col-md-6">
                    @if($errors->any())
                        <div class="alert alert-danger">

                            @foreach($errors->all() as $error)
                                {{$error}}
                            @endforeach


                        </div>
                    @endif

                    @if(session('message'))
                        <div class="alert alert-success">
                            {{session('message')}}
                        </div>
                @endif
                <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Banners</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        {!! Form::open(array('url' => 'admin','method' => 'post')) !!}

                            <div class="form-group">

                                {{ Form::label("name", null, ['class' => 'control-label']) }}


                                {{Form::text('name',null,["class" => "form-control",
            "placeholder" => "name ","id"=>"name"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("email", null, ['class' => 'control-label']) }}


                                {{Form::text('email',null,["class" => "form-control",
            "placeholder" => "email","id"=>"email"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("password", null, ['class' => 'control-label']) }}


                                {{Form::password('password',null,["class" => "form-control",
            "placeholder" => "password ","id"=>"password"])}}

                            </div>
                        <div class="form-group">

                            {{ Form::label("phone", null, ['class' => 'control-label']) }}


                            {{Form::text('phone',null,["class" => "form-control",
        "placeholder" => "phone","id"=>"phone"])}}

                        </div>

                            <div class="form-group">
                                {{ Form::label(" Is drafted", null, ['class' => 'control-label']) }}
                                <br>

                                {{Form::radio("is_draft","1")}} yes

                                {{Form::radio("is_draft","0")}} no
                            </div>
                            <div class="form-group">
                                {{ Form::label(" soft delete", null, ['class' => 'control-label']) }}
                                <br>

                                {{Form::radio("soft_delete","1")}} yes

                                {{Form::radio("soft_delete","0")}} no
                            </div>



                            <!-- /.card-body -->

                            <div class="card-footer">
                                {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card -->
                    </div>






    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{asset('back-end/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('back-end/plugins/bootstrap/js/bootstrap.bundle.js')}}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{asset('back-end/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });

@endsection;

