@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')



@section('content')


    <!-- Main content -->
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        @if($errors->any())
                            <div class="alert alert-danger">

                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach


                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                    @endif
                    <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">cart</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            {!! Form::open(array('url' => 'cart','method' => 'post', 'enctype'=>"multipart/form-data")) !!}

                            <div class="form-group">

                                {{ Form::label("sid", null, ['class' => 'control-label']) }}


                                {{Form::number('sid',null,["class" => "form-control",
            "placeholder" => "sid ","id"=>"sid"])}}

                            </div>

                            <div class="form-group">

                                {{ Form::label("product_id", null, ['class' => 'control-label']) }}


                                {{Form::number('product_id',null,["class" => "form-control",
            "placeholder" => "product_id ","id"=>"product_id"])}}

                            </div>
                            <div class="form-group">
                                {{ Form::label("picture", null, ['class' => 'control-label']) }}
                                <br>

                                {{Form::file("picture",null,["class" => "form-control",]) }}


                            </div>
                            <div class="form-group">

                                {{ Form::label("product_title", null, ['class' => 'control-label']) }}


                                {{Form::text('product_title',null,["class" => "form-control",
            "placeholder" => "product_title","id"=>"product_title"])}}

                            </div>


                            <div class="form-group">

                                {{ Form::label("qty", null, ['class' => 'control-label']) }}


                                {{Form::number('qty',null,["class" => "form-control",
            "placeholder" => "qty ","id"=>"qty"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("unite_price", null, ['class' => 'control-label']) }}


                                {{Form::number('unite_price',null,["class" => "form-control",
            "placeholder" => "unite_price ","id"=>"unite_price"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("total_price", null, ['class' => 'control-label']) }}


                                {{Form::number('total_price',null,["class" => "form-control",
            "placeholder" => "total_price ","id"=>"total_price"])}}

                            </div>



                            <!-- /.card-body -->

                            <div class="card-footer">
                                {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                            </div>
                            {!! Form::close() !!}
                        </div>
                        <!-- /.card -->
                    </div>






        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    <script src="{{asset('back-end/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('back-end/plugins/bootstrap/js/bootstrap.bundle.js')}}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{asset('back-end/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });

        @endsection;

