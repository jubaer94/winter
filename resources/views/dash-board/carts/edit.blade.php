@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')



@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        @if($errors->any())
                            <div class="alert alert-danger">

                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach


                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                    @endif
                    <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">carts</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            {!! Form::open(array('url' => "cart/$cart->id",'method' => 'put','enctype'=>"multipart/form-data")) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    {{ Form::label("picture", null, ['class' => 'control-label']) }}
                                    <br>
                                    <td><img src="{{asset("images/carts/$cart->picture")}}" alt="HTML5 Icon" style="width:150px;height:150px" ></td>
                                    <br>

                                    {{Form::file("picture",null,["class" => "form-control",]) }}


                                </div>


                            </div>
                            <div class="form-group">

                                {{ Form::label("sid", null, ['class' => 'control-label']) }}


                                {{Form::number('sid', old('sid')? old('sid'):(!empty($cart)?$cart->sid:null),["class" => "form-control",
            "placeholder" => "sid ","id"=>"sid"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("product_id", null, ['class' => 'control-label']) }}


                                {{Form::number('product_id', old('product_id')? old('product_id'):(!empty($cart)?$cart->product_id:null),["class" => "form-control",
            "placeholder" => "product_id ","id"=>"product_id"])}}

                            </div>

                            <div class="form-group">

                                {{ Form::label("product_title", null, ['class' => 'control-label']) }}


                                {{Form::text('product_title', old('product_title')? old('product_title'):(!empty($cart)?$cart->product_title:null),["class" => "form-control",
            "placeholder" => "product_title ","id"=>"product_title"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("qty", null, ['class' => 'control-label']) }}


                                {{Form::number('qty', old('qty')? old('qty'):(!empty($cart)?$cart->qty:null),["class" => "form-control",
            "placeholder" => "qty ","id"=>"qty"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("unite_price", null, ['class' => 'control-label']) }}


                                {{Form::number('unite_price', old('unite_price')? old('unite_price'):(!empty($cart)?$cart->unite_price:null),["class" => "form-control",
            "placeholder" => "unite_price ","id"=>"unite_price"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("total_price", null, ['class' => 'control-label']) }}


                                {{Form::number('total_price', old('total_price')? old('total_price'):(!empty($cart)?$cart->total_price:null),["class" => "form-control",
            "placeholder" => "total_price ","id"=>"total_price"])}}

                            </div>
{{--                           d-body -->--}}

                            <div class="card-footer">
                                {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.card -->
