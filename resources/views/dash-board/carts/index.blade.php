@extends('layouts.backendmaster')


@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')


{{--<div class="col-md-6">--}}

{{--    <table class="table table-hover table-striped">--}}


{{--<tr>--}}
{{--    <th>brand name</th>--}}
{{--    <th>brand title</th>--}}
{{--    <th>actions</th>--}}
{{--</tr>--}}
{{--        @foreach( $carts as $key => $cart)--}}
{{--    <tr>--}}
{{--        <td>{{$carts->firstItem()+ $key}}</td>--}}
{{--        <td>{{$cart->name}}</td>--}}
{{--        <td>{{$cart->title}}</td>--}}
{{--        <td>--}}
{{--            <a href="{{url("banners/$cart->id")}}">show</a>--}}
{{--            <a href="{{url("banners/$cart->id/edit")}}">edit</a>--}}
{{--            edit | delete | show--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--            @endforeach--}}
{{--    </table>--}}
{{--    {{$carts->links()}}--}}
{{--</div>--}}
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('cart/create')}}">CREATE</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable of banners</h3>
                        </div>
                        <div class="col-md-6">
                            @if($errors->any())
                                <div class="alert alert-danger">

                                    @foreach($errors->all() as $error)
                                        {{$error}}
                                    @endforeach


                                </div>
                            @endif

                            @if(session('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                        @endif
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th>picture</th>
                                        <th>sid</th>
                                        <th>product_id</th>
                                        <th>product_title</th>
                                        <th>quantity</th>
                                        <th>unite price</th>
                                        <th>total price</th>
                                        <th>max_display</th>
                                        <th>actions</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($carts as $key => $cart)
                                        <tr>
                                            <td>{{$carts->firstItem()+ $key}}</td>
                                            <td><img src="{{asset("images/carts/$cart->picture")}}" alt="HTML5 Icon" style="width:150px;height:150px" ></td>
                                            <td>{{$cart->sid}}</td>
                                            <td>{{$cart->product_id}}</td>
                                            <td>{{$cart->product_title}}</td>
                                            <td>{{$cart->qty}}</td>
                                            <td>{{$cart->unite_price}}</td>
                                            <td>{{$cart->total_price}}</td>


                                            <td>
                                                <a href="{{url("cart/$cart->id")}}" class="btn" type="submit">show</a>
                                                <a href="{{url("cart/$cart->id/edit")}} " class="btn" type="submit">edit</a>


                                                {!! Form::open(array('url' => "cart/$cart->id",'method' => 'delete' , 'class'=>'pull-right')) !!}
                                                {{Form::submit('delete', ['class' =>'btn btn-danger'])}}

                                                {!! Form::close() !!}


                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>

                                </table>
                                {{$carts->links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        @endsection;

