@extends('layouts.backendmaster')


@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')


{{--<div class="col-md-6">--}}

{{--    <table class="table table-hover table-striped">--}}


{{--<tr>--}}
{{--    <th>brand name</th>--}}
{{--    <th>brand title</th>--}}
{{--    <th>actions</th>--}}
{{--</tr>--}}
{{--        @foreach( $banners as $key => $banner)--}}
{{--    <tr>--}}
{{--        <td>{{$banners->firstItem()+ $key}}</td>--}}
{{--        <td>{{$banner->name}}</td>--}}
{{--        <td>{{$banner->title}}</td>--}}
{{--        <td>--}}
{{--            <a href="{{url("banners/$banner->id")}}">show</a>--}}
{{--            <a href="{{url("banners/$banner->id/edit")}}">edit</a>--}}
{{--            edit | delete | show--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--            @endforeach--}}
{{--    </table>--}}
{{--    {{$banners->links()}}--}}
{{--</div>--}}
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('banners/create')}}">CREATE</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable of banners</h3>
                        </div>
                        <div class="col-md-6">
                            @if($errors->any())
                                <div class="alert alert-danger">

                                    @foreach($errors->all() as $error)
                                        {{$error}}
                                    @endforeach


                                </div>
                            @endif

                            @if(session('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                        @endif
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th>picture</th>
                                        <th>link</th>
                                        <th>promotional message</th>
                                        <th>html_banner</th>
                                        <th>is_active</th>
                                        <th>is_draft</th>
                                        <th>soft_delete</th>
                                        <th>max_display</th>
                                        <th>actions</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($banners as $key => $banner)
                                        <tr>
                                            <td>{{$banners->firstItem()+ $key}}</td>
                                            <td>{{$banner->title}}</td>
                                            <td><img src="{{asset("images/Banners/$banner->picture")}}" alt="HTML5 Icon" style="width:150px;height:150px" ></td>
                                            <td>{{$banner->link}}</td>
                                            <td>{{$banner->promotional_message}}</td>
                                            <td>{{$banner->html_banner}}</td>
                                            <td>{{$banner->is_active}}</td>
                                            <td>{{$banner->is_draft}}</td>
                                            <td>{{$banner->soft_delete}}</td>
                                            <td>{{$banner->max_display}}</td>

                                            <td>
                                                <a href="{{url("banners/$banner->id")}}" class="btn" type="submit">show</a>
                                                <a href="{{url("banners/$banner->id/edit")}} " class="btn" type="submit">edit</a>


                                                {!! Form::open(array('url' => "banners/$banner->id",'method' => 'delete' , 'class'=>'pull-right')) !!}
                                                {{Form::submit('delete', ['class' =>'btn btn-danger'])}}

                                                {!! Form::close() !!}


                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th> #</th>
                                        <th>brand</th>
                                        <th>name</th>
                                        <th>actions</th>

                                    </tr>
                                    </tfoot>
                                </table>
                                {{$banners->links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        @endsection;

