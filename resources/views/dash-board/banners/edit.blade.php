@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')



@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        @if($errors->any())
                            <div class="alert alert-danger">

                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach


                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                    @endif
                    <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">banners</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            {!! Form::open(array('url' => "banners/$banner->id",'method' => 'put','enctype'=>"multipart/form-data")) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    {{ Form::label("picture", null, ['class' => 'control-label']) }}
                                    <br>
                                    <td><img src="{{asset("images/Banners/$banner->picture")}}" alt="HTML5 Icon" style="width:150px;height:150px" ></td>
                                    <br>

                                    {{Form::file("picture",null,["class" => "form-control",]) }}


                                </div>


                                </div>
                            <div class="form-group">

                                {{ Form::label("title", null, ['class' => 'control-label']) }}


                                {{Form::text('title', old('title')? old('title'):(!empty($brand)?$brand->title:null),["class" => "form-control",
            "placeholder" => "title ","id"=>"title"])}}

                            </div>
                                <div class="form-group">

                                    {{ Form::label("link", null, ['class' => 'control-label']) }}


                                    {{Form::text('link', old('link')? old('link'):(!empty($banner)?$banner->link:null),["class" => "form-control",
                "placeholder" => "link ","id"=>"link"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("promotional_message", null, ['class' => 'control-label']) }}


                                    {{Form::text('promotional_message', old('promotional_message')? old('promotional_message'):(!empty($banner)?$banner->promotional_message:null),["class" => "form-control",
                "placeholder" => "promotional_message ","id"=>"promotional_message"])}}

                                </div>

                                <div class="form-group">

                                    {{ Form::label("html_banner", null, ['class' => 'control-label']) }}


                                    {{Form::text('html_banner', old('html_banner')? old('html_banner'):(!empty($banner)?$banner->html_banner:null),["class" => "form-control",
                "placeholder" => "html banner ","id"=>"html_banner"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("html_banner", null, ['class' => 'control-label']) }}


                                    {{Form::text('html_banner', old('html_banner')? old('html_banner'):(!empty($banner)?$banner->html_banner:null),["class" => "form-control",
                "placeholder" => "html banner ","id"=>"html_banner"])}}

                                </div>
                                <div class="form-group">
                                    {{ Form::label(" active status", null, ['class' => 'control-label']) }}
                                    <br>
                                    @if($banner->is_active==1)
                                    {{Form::radio("is_active","1",['checked=>checked'])}} active

                                    {{Form::radio("is_active","0")}} inactive
                                        @else
                                        {{Form::radio("is_active","1")}} active

                                        {{Form::radio("is_active","0",['checked=>checked'])}} inactive
                                        @endif

                                </div>
                                <div class="form-group">
                                    {{ Form::label("is drafted", null, ['class' => 'control-label']) }}
                                    <br>
                                    @if($banner->is_draft==1)
                                        {{Form::radio("is_draft","1",['checked=>checked'])}} yes

                                        {{Form::radio("is_draft","0")}} no
                                    @else
                                        {{Form::radio("is_draft","1")}} yes

                                        {{Form::radio("is_draft","0",['checked=>checked'])}} no
                                    @endif

                                </div>
                                <div class="form-group">
                                    {{ Form::label("soft delete", null, ['class' => 'control-label']) }}
                                    <br>
                                    @if($banner->is_draft==1)
                                        {{Form::radio("soft_delete","1",['checked=>checked'])}} active

                                        {{Form::radio("soft_deletet","0")}} inactive
                                    @else
                                        {{Form::radio("soft_delete","1")}} active

                                        {{Form::radio("soft_delete","0",['checked=>checked'])}} inactive
                                    @endif

                                </div>
                                <div class="form-group">

                                    {{ Form::label("Max display", null, ['class' => 'control-label']) }}


                                    {{Form::number('max_display',old('max_display')?old('max_display'):(!empty($banner)?$banner->max_display:null),["class" => "form-control",
                "placeholder" => "max_display ","id"=>"max_display"])}}

                                </div>
                                <!-- /.card-body -->

                                <div class="card-footer">
                                    {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                    {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- /.card -->

