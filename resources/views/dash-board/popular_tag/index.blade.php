@extends('layouts.backendmaster')


@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')


{{--<div class="col-md-6">--}}

{{--    <table class="table table-hover table-striped">--}}


{{--<tr>--}}
{{--    <th>brand name</th>--}}
{{--    <th>brand title</th>--}}
{{--    <th>actions</th>--}}
{{--</tr>--}}
{{--        @foreach( $populartag as $key => $populartag)--}}
{{--    <tr>--}}
{{--        <td>{{$populartag->firstItem()+ $key}}</td>--}}
{{--        <td>{{$populartag->name}}</td>--}}
{{--        <td>{{$populartag->title}}</td>--}}
{{--        <td>--}}
{{--            <a href="{{url("populartag/$populartag->id")}}">show</a>--}}
{{--            <a href="{{url("populartag/$populartag->id/edit")}}">edit</a>--}}
{{--            edit | delete | show--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--            @endforeach--}}
{{--    </table>--}}
{{--    {{$populartag->links()}}--}}
{{--</div>--}}
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('populartag/create')}}">CREATE</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable of populartag</h3>
                        </div>
                        <div class="col-md-6">
                            @if($errors->any())
                                <div class="alert alert-danger">

                                    @foreach($errors->all() as $error)
                                        {{$error}}
                                    @endforeach


                                </div>
                            @endif

                            @if(session('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                        @endif
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th>name</th>
                                        <th>link</th>
                                        <th>is_active</th>
                                        <th>is_draft</th>
                                        <th>soft_delete</th>
                                        <th>actions</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($populartags as $key => $populartag)
                                        <tr>
                                            <td>{{$populartags->firstItem()+ $key}}</td>
                                            <td>{{$populartag->name}}</td>
                                            <td>{{$populartag->link}}</td>
                                            <td>{{$populartag->is_active}}</td>
                                            <td>{{$populartag->is_draft}}</td>
                                            <td>{{$populartag->soft_delete}}</td>


                                            <td>
                                                <a href="{{url("populartag/$populartag->id")}}" class="btn" type="submit">show</a>
                                                <a href="{{url("populartag/$populartag->id/edit")}} " class="btn" type="submit">edit</a>


                                                {!! Form::open(array('url' => "populartag/$populartag->id",'method' => 'delete' , 'class'=>'pull-right')) !!}
                                                {{Form::submit('delete', ['class' =>'btn btn-danger'])}}

                                                {!! Form::close() !!}


                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>

                                </table>
                                {{$populartags->links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        @endsection;

