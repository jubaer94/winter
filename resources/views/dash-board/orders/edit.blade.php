@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')



@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        @if($errors->any())
                            <div class="alert alert-danger">

                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach


                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                    @endif
                    <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">orders</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            {!! Form::open(array('url' => "order/$order->id",'method' => 'put')) !!}

                            <div class="form-group">

                                {{ Form::label("product_id", null, ['class' => 'control-label']) }}


                                {{Form::number('product_id', old('product_id')? old('product_id'):(!empty($order)?$order->product_id:null),["class" => "form-control",
            "placeholder" => "product_id ","id"=>"product_id"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("quantity", null, ['class' => 'control-label']) }}


                                {{Form::number('qty', old('qty')? old('qty'):(!empty($order)?$order->qty:null),["class" => "form-control",
            "placeholder" => "qty ","id"=>"qty"])}}

                            </div>

                            <!-- /.card-body -->

                            <div class="card-footer">
                                {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.card -->

                </div>
            </div>
        </section>
