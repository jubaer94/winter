@extends('layouts.backendmaster')


@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')


{{--<div class="col-md-6">--}}

{{--    <table class="table table-hover table-striped">--}}


{{--<tr>--}}
{{--    <th>brand name</th>--}}
{{--    <th>brand title</th>--}}
{{--    <th>actions</th>--}}
{{--</tr>--}}
{{--        @foreach( $productlistss as $key => $productlists)--}}
{{--    <tr>--}}
{{--        <td>{{$productlistss->firstItem()+ $key}}</td>--}}
{{--        <td>{{$productlists->name}}</td>--}}
{{--        <td>{{$productlists->title}}</td>--}}
{{--        <td>--}}
{{--            <a href="{{url("banners/$productlists->id")}}">show</a>--}}
{{--            <a href="{{url("banners/$productlists->id/edit")}}">edit</a>--}}
{{--            edit | delete | show--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--            @endforeach--}}
{{--    </table>--}}
{{--    {{$productlistss->links()}}--}}
{{--</div>--}}
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('productlist/create')}}">CREATE</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-auto">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable of productlist</h3>
                        </div>
                        <div class="col-md-6">
                            @if($errors->any())
                                <div class="alert alert-danger">

                                    @foreach($errors->all() as $error)
                                        {{$error}}
                                    @endforeach


                                </div>
                            @endif

                            @if(session('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                        @endif
                                <div class="wrapper">
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-responsive">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                         <th>ID</th>
                                        <th>is_draft</th>
                                        <th>is_active</th>
                                        <th>name</th>
                                        <th>title</th>
                                        <th>brand_id</th>
                                        <th>label_id</th>
                                        <th>picture</th>
                                        <th>short_description</th>
                                        <th>description</th>
                                        <th>total_sales</th>
                                        <th>is_new</th>
                                        <th>cost</th>
                                        <th>mrp</th>
                                        <th>special price</th>
                                        <th>soft delete</th>
                                        <th>categorie_id</th>
                                        <th>actions</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($productlist as $key => $productlists)
                                        <tr>
{{--                                             <td>{{$productlists->brand->name}}</td>--}}
                                            <td>{{$productlist->firstItem()+ $key}}</td>
                                            <td>{{$productlists->id}}</td>
                                            <td>{{$productlists->is_draft}}</td>
                                            <td>{{$productlists->is_active}}</td>
                                            <td>{{$productlists->name}}</td>
                                            <td>{{$productlists->title}}</td>
                                            <td>{{$productlists->brand_id}}</td>
                                            <td>{{$productlists->label_id}}</td>
                                            <td><img src="{{asset("images/productlist/$productlists->picture")}}" alt="HTML5 Icon" style="width:150px;height:150px" ></td>
                                            <td>{{$productlists->short_description}}</td>
                                            <td>{{$productlists->description}}</td>
                                            <td>{{$productlists->total_sales}}</td>
                                            <td>{{$productlists->is_new}}</td>
                                            <td>{{$productlists->cost}}</td>
                                            <td>{{$productlists->mrp}}</td>
                                            <td>{{$productlists->is_active}}</td>
                                            <td>{{$productlists->soft_delete}}</td>
                                            <td>{{$productlists->category_id}} </td>
                                            <td>
                                                <a href="{{url("productlist/$productlists->id")}}" class="btn" type="submit">show</a>
                                                <a href="{{url("productlist/$productlists->id/edit")}} " class="btn" type="submit">edit</a>


                                                {!! Form::open(array('url' => "productlist/$productlists->id",'method' => 'delete' , 'class'=>'pull-right')) !!}
                                                {{Form::submit('delete', ['class' =>'btn btn-danger'])}}

                                                {!! Form::close() !!}

                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>

                                </table>
                                {{$productlist->links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
            </div>
            </div>
        </section>
                        @endsection;

