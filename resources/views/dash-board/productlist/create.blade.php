@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')



@section('content')


    <!-- Main content -->
    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        @if($errors->any())
                            <div class="alert alert-danger">

                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach


                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                    @endif
                    <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Productlist</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            {!! Form::open(array('url' => 'productlist','method' => 'post' , 'enctype'=>"multipart/form-data")) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    {{ Form::label("name", null, ['class' => 'control-label']) }}

                                    {{Form::text('name',null,["class" => "form-control",
                "namer" => "name","id"=>"name"])}}


                                </div>
                                <div class="form-group">

                                    {{ Form::label("title", null, ['class' => 'control-label']) }}


                                    {{Form::text('title',null,["class" => "form-control",
                "placeholder" => "title ","id"=>"title"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("brand_id", null, ['class' => 'control-label']) }}


                                    {{Form::number('brand_id',null,["class" => "form-control",
                "placeholder" => "brand_id","id"=>"brand_id"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("category_id", null, ['class' => 'control-label']) }}


                                    {{Form::number('category_id',null,["class" => "form-control",
                "placeholder" => "category_id","id"=>"category_id"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("label_id", null, ['class' => 'control-label']) }}


                                    {{Form::number('label_id',null,["class" => "form-control",
                "placeholder" => "label_id","id"=>"label_id"])}}

                                </div>

                                    <div class="form-group">
                                        {{ Form::label("picture", null, ['class' => 'control-label']) }}
                                        <br>

                                        {{Form::file("picture",null,["class" => "form-control",]) }}


                                    </div>
                                <div class="form-group">

                                    {{ Form::label("short_description", null, ['class' => 'control-label']) }}


                                    {{Form::text('short_description',null,["class" => "form-control",
                "placeholder" => "short_description","id"=>"short_description"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("description", null, ['class' => 'control-label']) }}


                                    {{Form::text('description',null,["class" => "form-control",
                "placeholder" => "description","id"=>"description"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("total_sales", null, ['class' => 'control-label']) }}


                                    {{Form::number('total_sales',null,["class" => "form-control",
                "placeholder" => "total_sales","id"=>"total_sales"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("cost", null, ['class' => 'control-label']) }}


                                    {{Form::number('cost',null,["class" => "form-control",
                "placeholder" => "cost","id"=>"cost"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("mrp", null, ['class' => 'control-label']) }}


                                    {{Form::number('mrp',null,["class" => "form-control",
                "placeholder" => "mrp","id"=>"mrp"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("special_price", null, ['class' => 'control-label']) }}


                                    {{Form::number('special_price',null,["class" => "form-control",
                "placeholder" => "special_price","id"=>"special_price"])}}

                                </div>
                                <div class="form-group">
                                    {{ Form::label(" is_new", null, ['class' => 'control-label']) }}
                                    <br>

                                    {{Form::radio("is_new","1")}} yes

                                    {{Form::radio("is_new","0")}} no
                                </div>
                                <div class="form-group">
                                    {{ Form::label(" active status", null, ['class' => 'control-label']) }}
                                    <br>

                                    {{Form::radio("is_active","1")}} active

                                    {{Form::radio("is_active","0")}} inactive
                                </div>
                                <div class="form-group">
                                    {{ Form::label(" Is drafted", null, ['class' => 'control-label']) }}
                                    <br>

                                    {{Form::radio("is_draft","1")}} yes

                                    {{Form::radio("is_draft","0")}} no
                                </div>
                                <div class="form-group">
                                    {{ Form::label(" soft delete", null, ['class' => 'control-label']) }}
                                    <br>

                                    {{Form::radio("soft_delete","1")}} yes

                                    {{Form::radio("soft_delete","0")}} no
                                </div>


                                <!-- /.card-body -->

                                <div class="card-footer">
                                    {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                    {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                                </div>
                                {!! Form::close() !!}
                            </div>
                            <!-- /.card -->
                        </div>




        </section>
    </div>


        <!-- /.content -->
    </div>

    <!-- /.content-wrapper -->
    <script src="{{asset('back-end/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('back-end/plugins/bootstrap/js/bootstrap.bundle.js')}}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{asset('back-end/plugins/bs-custom-file-input/bs-custom-file-input.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('dist/js/adminlte.min.js')}}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{asset('dist/js/demo.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            bsCustomFileInput.init();
        });
        </script>

        @endsection;

