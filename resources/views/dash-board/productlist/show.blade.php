@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('productlist')}}">productlist</a></li>
                            {{--                    <li class="breadcrumb-item active">DataTables</li>--}}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="col-md-6">
                @if($errors->any())
                    <div class="alert alert-danger">

                        @foreach($errors->all() as $error)
                            {{$error}}
                        @endforeach


                    </div>
                @endif

                @if(session('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif

            </div>
            <div class="content-wrapper">


                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Bordered Table</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>

                                            <th>Field Name</th>
                                            <th>Data</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>#.ID</td>
                                            <td>{{$productlist->id}}</td>

                                        </tr>
                                        <tr>
                                            <td>#.brand name</td>
                                            <td>{{$productlist->brand->title}}</td>


                                        </tr>
                                        <tr>
                                            <td>1.name</td>
                                            <td>{{$productlist->name}}</td>

                                        </tr>
                                        <tr>
                                            <td>2.title</td>
                                            <td>{{$productlist->title}}</td>

                                        </tr>
                                        <tr>
                                            <td>3.brand_id</td>
                                            <td>{{$productlist->brand_id}}</td>

                                        </tr>
                                        <tr>
                                            <td>4.label_id</td>
                                            <td>{{$productlist->label_id}}</td>

                                        </tr>
                                        <tr>
                                            <td>2.picture</td>
                                            <td><img src="{{asset("images/productlist/$productlist->picture")}}" alt="HTML5 Icon" style="width:150px;height:150px" ></td>

                                        </tr>
                                        <tr>
                                            <td>6.short_description</td>
                                            <td>{{$productlist->short_description}}</td>

                                        </tr>
                                        <tr>
                                            <td>7.total_sales</td>
                                            <td>{{$productlist->total_sales}}</td>

                                        </tr>
                                        <tr>
                                            <td>8.soft delete</td>
                                            <td>{{$productlist->soft_delete}}</td>

                                              <tr>
                                            <td>9.is_new</td>
                                            <td>{{$productlist->is_new}}</td>
                                        </tr>

                                        </tr>
                                        <tr>
                                            <td>10.cost</td>
                                            <td>{{$productlist->cost}}</td>

                                        </tr>
                                        <tr>
                                            <td>11.mrp</td>
                                            <td>{{$productlist->mrp}}</td>

                                        </tr>
                                        <tr>
                                            <td>12.special price</td>
                                            <td>{{$productlist->Special_price}}</td>

                                        </tr>
                                        <tr>
                                            <td>13.soft delete</td>
                                            <td>{{$productlist->soft_delete}}</td>

                                        </tr>
                                        <tr>
                                            <td>14.is active</td>
                                            <td>{{$productlist->is_active}}</td>

                                        </tr>
                                        <tr>
                                        <td>15.is draft</td>
                                        <td>{{$productlist->is_draft}}</td>
                                        </tr>
                                        <tr>
                                            <td>16.category id</td>
                                            <td>{{$productlist->category_id}}</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                {{--                                    <tr>--}}
                                {{--                                        <td>2.</td>--}}
                                {{--                                        <td>Clean database</td>--}}
                                {{--                                        <td>--}}
                                {{--                                            <div class="progress progress-xs">--}}
                                {{--                                                <div class="progress-bar bg-warning" style="width: 70%"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </td>--}}
                                {{--                                        <td><span class="badge bg-warning">70%</span></td>--}}
                                {{--                                    </tr>--}}
                                {{--                                    <tr>--}}
                                {{--                                        <td>3.</td>--}}
                                {{--                                        <td>Cron job running</td>--}}
                                {{--                                        <td>--}}
                                {{--                                            <div class="progress progress-xs progress-striped active">--}}
                                {{--                                                <div class="progress-bar bg-primary" style="width: 30%"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </td>--}}
                                {{--                                        <td><span class="badge bg-primary">30%</span></td>--}}
                                {{--                                    </tr>--}}
                                {{--                                    <tr>--}}
                                {{--                                        <td>4.</td>--}}
                                {{--                                        <td>Fix and squish bugs</td>--}}
                                {{--                                        <td>--}}
                                {{--                                            <div class="progress progress-xs progress-striped active">--}}
                                {{--                                                <div class="progress-bar bg-success" style="width: 90%"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </td>--}}
                                {{--                                        <td><span class="badge bg-success">90%</span></td>--}}
                                {{--                                    </tr>--}}
                                {{--                                    </tbody>--}}
                                {{--                                </table>--}}
                                {{--                            </div>--}}
                                <!-- /.card-body -->

                                </div>
                                <!-- /.card -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection

