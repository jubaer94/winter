@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')



@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        @if($errors->any())
                            <div class="alert alert-danger">

                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach


                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                    @endif
                    <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">contacts</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            {!! Form::open(array('url' => "contact/$contact->id",'method' => 'put')) !!}
                            <div class="card-body">
                                <div class="form-group">
                                    {{ Form::label("name", null, ['class' => 'control-label']) }}

                                    {{-- <label for="name">name</label>--}}
                                    {{--                                    <input type="text" class="form-control" id="name" placeholder="enter your name">--}}
                                    {{Form::text('name',old('name')? old('name'):(!empty($contact)? $contact->name:null),["class" => "form-control",
                "placeholder" => "name","id"=>"name"])}}


                                </div>
                                <div class="form-group">

                                    {{ Form::label("email", null, ['class' => 'control-label']) }}


                                    {{Form::text('email', old('email')? old('email'):(!empty($contact)?$contact->email:null),["class" => "form-control",
                "placeholder" => "email ","id"=>"email"])}}

                                </div>


                                <div class="form-group">

                                    {{ Form::label("subject", null, ['class' => 'control-label']) }}


                                    {{Form::text('subject', old('subject')? old('subject'):(!empty($contact)?$contact->subject:null),["class" => "form-control",
                "placeholder" => "subject ","id"=>"subject"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("comment", null, ['class' => 'control-label']) }}


                                    {{Form::text('comment', old('comment')? old('comment'):(!empty($contact)?$contact->comment:null),["class" => "form-control",
                "placeholder" => "comment ","id"=>"comment"])}}

                                </div>


                                <div class="form-group">
                                    {{ Form::label("status", null, ['class' => 'control-label']) }}
                                    <br>
                                    @if($contact->status==1)
                                        {{Form::radio("status","1",['checked=>checked'])}} active

                                        {{Form::radio("status","0")}} inactive
                                    @else
                                        {{Form::radio("status","1")}} active

                                        {{Form::radio("status","0",['checked=>checked'])}} inactive
                                    @endif

                                </div>
                                <div class="form-group">
                                    {{ Form::label("soft delete", null, ['class' => 'control-label']) }}
                                    <br>
                                    @if($contact->soft_delete==1)
                                        {{Form::radio("soft_delete","1",['checked=>checked'])}} active

                                        {{Form::radio("soft_deletet","0")}} inactive
                                    @else
                                        {{Form::radio("soft_delete","1")}} active

                                        {{Form::radio("soft_delete","0",['checked=>checked'])}} inactive
                                    @endif

                                </div>

                                <!-- /.card-body -->

                                <div class="card-footer">
                                    {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                    {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                                </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- /.card -->

