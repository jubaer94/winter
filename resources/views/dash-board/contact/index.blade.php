@extends('layouts.backendmaster')


@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')


{{--<div class="col-md-6">--}}

{{--    <table class="table table-hover table-striped">--}}


{{--<tr>--}}
{{--    <th>brand name</th>--}}
{{--    <th>brand title</th>--}}
{{--    <th>actions</th>--}}
{{--</tr>--}}
{{--        @foreach( $banners as $key => $banner)--}}
{{--    <tr>--}}
{{--        <td>{{$banners->firstItem()+ $key}}</td>--}}
{{--        <td>{{$banner->name}}</td>--}}
{{--        <td>{{$banner->title}}</td>--}}
{{--        <td>--}}
{{--            <a href="{{url("banners/$banner->id")}}">show</a>--}}
{{--            <a href="{{url("banners/$banner->id/edit")}}">edit</a>--}}
{{--            edit | delete | show--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--            @endforeach--}}
{{--    </table>--}}
{{--    {{$banners->links()}}--}}
{{--</div>--}}
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('contact/create')}}">CREATE</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable of admin</h3>
                        </div>
                        <div class="col-md-6">
                            @if($errors->any())
                                <div class="alert alert-danger">

                                    @foreach($errors->all() as $error)
                                        {{$error}}
                                    @endforeach


                                </div>
                            @endif

                            @if(session('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                        @endif
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th>name</th>
                                        <th>email</th>
                                        <th>subject</th>
                                        <th>comment</th>
                                        <th>status</th>
                                        <th>soft_delete</th>
                                        <th>actions</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($contacts as $key => $contact)
                                        <tr>
                                            <td>{{$contacts->firstItem()+ $key}}</td>
                                            <td>{{$contact->name}}</td>
                                            <td>{{$contact->email}}</td>
                                            <td>{{$contact->subject}}</td>
                                            <td>{{$contact->comment}}</td>
                                            <td>{{$contact->status}}</td>
                                            <td>{{$contact->soft_delete}}</td>


                                            <td>
                                                <a href="{{url("contact/$contact->id")}}" class="btn" type="submit">show</a>
                                                <a href="{{url("contact/$contact->id/edit")}} " class="btn" type="submit">edit</a>


                                                {!! Form::open(array('url' => "contact/$contact->id",'method' => 'delete' , 'class'=>'pull-right')) !!}
                                                {{Form::submit('delete', ['class' =>'btn btn-danger'])}}

                                                {!! Form::close() !!}


                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>
{{--                                    <tfoot>--}}
{{--                                    <tr>--}}
{{--                                        <th> #</th>--}}
{{--                                        <th>brand</th>--}}
{{--                                        <th>name</th>--}}
{{--                                        <th>actions</th>--}}

{{--                                    </tr>--}}
{{--                                    </tfoot>--}}
                                </table>
                                {{$contacts->links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        @endsection;

