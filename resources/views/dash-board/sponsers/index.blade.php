@extends('layouts.backendmaster')


@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')


{{--<div class="col-md-6">--}}

{{--    <table class="table table-hover table-striped">--}}


{{--<tr>--}}
{{--    <th>brand name</th>--}}
{{--    <th>brand title</th>--}}
{{--    <th>actions</th>--}}
{{--</tr>--}}
{{--        @foreach( $sponser as $key => $sponser)--}}
{{--    <tr>--}}
{{--        <td>{{$sponser->firstItem()+ $key}}</td>--}}
{{--        <td>{{$sponser->name}}</td>--}}
{{--        <td>{{$sponser->title}}</td>--}}
{{--        <td>--}}
{{--            <a href="{{url("sponser/$sponser->id")}}">show</a>--}}
{{--            <a href="{{url("sponser/$sponser->id/edit")}}">edit</a>--}}
{{--            edit | delete | show--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--            @endforeach--}}
{{--    </table>--}}
{{--    {{$sponser->links()}}--}}
{{--</div>--}}
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('sponser/create')}}">CREATE</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable of sponser</h3>
                        </div>
                        <div class="col-md-6">
                            @if($errors->any())
                                <div class="alert alert-danger">

                                    @foreach($errors->all() as $error)
                                        {{$error}}
                                    @endforeach


                                </div>
                            @endif

                            @if(session('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                        @endif
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th>title</th>
                                        <th>picture</th>
                                        <th>link</th>
                                        <th>promotional message</th>
                                        <th>html_banner</th>
                                        <th>is_active</th>
                                        <th>is_draft</th>
                                        <th>soft_delete</th>

                                        <th>actions</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($sponsers as $key => $sponser)
                                        <tr>
                                            <td>{{$sponsers->firstItem()+ $key}}</td>
                                            <td>{{$sponser->title}}</td>
                                            <td><img src="{{asset("images/sponsers/$sponser->picture")}}" alt="HTML5 Icon" style="width:150px;height:150px" ></td>
                                            <td>{{$sponser->link}}</td>
                                            <td>{{$sponser->promotional_message}}</td>
                                            <td>{{$sponser->html_banner}}</td>
                                            <td>{{$sponser->is_active}}</td>
                                            <td>{{$sponser->is_draft}}</td>
                                            <td>{{$sponser->soft_delete}}</td>


                                            <td>
                                                <a href="{{url("sponser/$sponser->id")}}" class="btn" type="submit">show</a>
                                                <a href="{{url("sponser/$sponser->id/edit")}} " class="btn" type="submit">edit</a>


                                                {!! Form::open(array('url' => "sponser/$sponser->id",'method' => 'delete' , 'class'=>'pull-right')) !!}
                                                {{Form::submit('delete', ['class' =>'btn btn-danger'])}}

                                                {!! Form::close() !!}


                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>

                                </table>
                                {{$sponsers->links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        @endsection;

