@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')



@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        @if($errors->any())
                            <div class="alert alert-danger">

                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach


                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                    @endif
                    <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">subscriber</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            {!! Form::open(array('url' => "subscriber/$subscriber->id",'method' => 'put','enctype'=>"multipart/form-data")) !!}
                            <div class="card-body">



                            </div>
                            <div class="form-group">

                                {{ Form::label("reason", null, ['class' => 'control-label']) }}


                                {{Form::textarea('reason', old('reason')? old('reason'):(!empty($subscriber)?$subscriber->reason:null),["class" => "form-control",
            "placeholder" => "reason ","id"=>"reason"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("email", null, ['class' => 'control-label']) }}


                                {{Form::text('email', old('email')? old('email'):(!empty($subscriber)?$subscriber->email:null),["class" => "form-control",
            "placeholder" => "email ","id"=>"email"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("title", null, ['class' => 'control-label']) }}


                                {{Form::number('is_subscribed', old('is_subscribed')? old('is_subscribed'):(!empty($subscriber)?$subscriber->is_subscribed:null),["class" => "form-control",
            "placeholder" => "title ","id"=>"is_subscribed"])}}

                            </div>






                            <!-- /.card-body -->

                            <div class="card-footer">
                                {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.card -->

