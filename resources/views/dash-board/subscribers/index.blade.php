@extends('layouts.backendmaster')


@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')


{{--<div class="col-md-6">--}}

{{--    <table class="table table-hover table-striped">--}}


{{--<tr>--}}
{{--    <th>subscriber name</th>--}}
{{--    <th>subscriber is_subscribed</th>--}}
{{--    <th>actions</th>--}}
{{--</tr>--}}
{{--        @foreach( $subscriber as $key => $subscriber)--}}
{{--    <tr>--}}
{{--        <td>{{$subscriber->firstItem()+ $key}}</td>--}}
{{--        <td>{{$subscriber->name}}</td>--}}
{{--        <td>{{$subscriber->is_subscribed}}</td>--}}
{{--        <td>--}}
{{--            <a href="{{url("banners/$subscriber->id")}}">show</a>--}}
{{--            <a href="{{url("banners/$subscriber->id/edit")}}">edit</a>--}}
{{--            edit | delete | show--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--            @endforeach--}}
{{--    </table>--}}
{{--    {{$subscriber->reasons()}}--}}
{{--</div>--}}
@section('content')
    <!-- Content Wrapper. Contains subscriber content -->
    <div class="content-wrapper">
        <!-- Content Header (subscriber header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('subscriber/create')}}">CREATE</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-is_subscribed">DataTable of banners</h3>
                        </div>
                        <div class="col-md-6">
                            @if($errors->any())
                                <div class="alert alert-danger">

                                    @foreach($errors->all() as $error)
                                        {{$error}}
                                    @endforeach


                                </div>
                            @endif

                            @if(session('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                        @endif
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th> #</th>
                                        <th>email</th>
                                        <th>reason</th>
                                        <th>is_subscribed</th>
                                        <th>actions</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($subscribers as $key => $subscriber)
                                        <tr>
                                            <td>{{$subscribers->firstItem()+ $key}}</td>
                                            <td>{{$subscriber->email}}</td>
                                            <td>{{$subscriber->reason}}</td>
                                            <td>{{$subscriber->is_subscribed}}</td>
                                            <td>
                                                <a href="{{url("subscriber/$subscriber->id")}}" class="btn" type="submit">show</a>
                                                <a href="{{url("subscriber/$subscriber->id/edit")}} " class="btn" type="submit">edit</a>


                                                {!! Form::open(array('url' => "subscriber/$subscriber->id",'method' => 'delete' , 'class'=>'pull-right')) !!}
                                                {{Form::submit('delete', ['class' =>'btn btn-danger'])}}

                                                {!! Form::close() !!}


                                            </td>
                                        </tr>

                                    @endforeach


                                    </tbody>

                                </table>
                                {{$subscribers->links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                        @endsection;

