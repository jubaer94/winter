@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')
@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('subscriber')}}">subscriber</a></li>
                            {{--                    <li class="breadcrumb-item active">DataTables</li>--}}
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">

            <div class="col-md-6">
                @if($errors->any())
                    <div class="alert alert-danger">

                        @foreach($errors->all() as $error)
                            {{$error}}
                        @endforeach


                    </div>
                @endif

                @if(session('message'))
                    <div class="alert alert-success">
                        {{session('message')}}
                    </div>
                @endif

            </div>
            <div class="content-wrapper">


                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">
                                    <h3 class="card-title">Bordered Table</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>

                                            <th>Field Name</th>
                                            <th>Data</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>1.email</td>
                                            <td>{{$subscriber->email}}</td>

                                        </tr>
                                        <tr>
                                            <td>2.reason</td>
                                            <td>{{$subscriber->reason}}</td>

                                        </tr>
                                        <tr>
                                            <td>3.is_subscribed</td>
                                            <td>{{$subscriber->is_subscribed}}</td>

                                        </tr>

                                        </tbody>
                                    </table>
                                {{--                                    <tr>--}}
                                {{--                                        <td>2.</td>--}}
                                {{--                                        <td>Clean database</td>--}}
                                {{--                                        <td>--}}
                                {{--                                            <div class="progress progress-xs">--}}
                                {{--                                                <div class="progress-bar bg-warning" style="width: 70%"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </td>--}}
                                {{--                                        <td><span class="badge bg-warning">70%</span></td>--}}
                                {{--                                    </tr>--}}
                                {{--                                    <tr>--}}
                                {{--                                        <td>3.</td>--}}
                                {{--                                        <td>Cron job running</td>--}}
                                {{--                                        <td>--}}
                                {{--                                            <div class="progress progress-xs progress-striped active">--}}
                                {{--                                                <div class="progress-bar bg-primary" style="width: 30%"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </td>--}}
                                {{--                                        <td><span class="badge bg-primary">30%</span></td>--}}
                                {{--                                    </tr>--}}
                                {{--                                    <tr>--}}
                                {{--                                        <td>4.</td>--}}
                                {{--                                        <td>Fix and squish bugs</td>--}}
                                {{--                                        <td>--}}
                                {{--                                            <div class="progress progress-xs progress-striped active">--}}
                                {{--                                                <div class="progress-bar bg-success" style="width: 90%"></div>--}}
                                {{--                                            </div>--}}
                                {{--                                        </td>--}}
                                {{--                                        <td><span class="badge bg-success">90%</span></td>--}}
                                {{--                                    </tr>--}}
                                {{--                                    </tbody>--}}
                                {{--                                </table>--}}
                                {{--                            </div>--}}
                                <!-- /.card-body -->

                                </div>
                                <!-- /.card -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>

@endsection

