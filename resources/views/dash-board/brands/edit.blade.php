@extends('layouts.backendmaster')
@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')



@section('content')
    <div class="content-wrapper">

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        @if($errors->any())
                            <div class="alert alert-danger">

                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach


                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                    @endif
                    <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">brands</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            {!! Form::open(array('url' => "brand/$brand->id",'method' => 'put','enctype'=>"multipart/form-data")) !!}
                            <div class="card-body">



                            </div>
                            <div class="form-group">

                                {{ Form::label("name", null, ['class' => 'control-label']) }}


                                {{Form::text('name', old('name')? old('name'):(!empty($brand)?$brand->name:null),["class" => "form-control",
            "placeholder" => "name ","id"=>"name"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("link", null, ['class' => 'control-label']) }}


                                {{Form::text('link', old('link')? old('link'):(!empty($brand)?$brand->link:null),["class" => "form-control",
            "placeholder" => "link ","id"=>"link"])}}

                            </div>
                            <div class="form-group">

                                {{ Form::label("title", null, ['class' => 'control-label']) }}


                                {{Form::text('title', old('title')? old('title'):(!empty($brand)?$brand->title:null),["class" => "form-control",
            "placeholder" => "title ","id"=>"title"])}}

                            </div>




                            <div class="form-group">
                                {{ Form::label(" active status", null, ['class' => 'control-label']) }}
                                <br>
                                @if($brand->is_active==1)
                                    {{Form::radio("is_active","1",['checked=>checked'])}} active

                                    {{Form::radio("is_active","0")}} inactive
                                @else
                                    {{Form::radio("is_active","1")}} active

                                    {{Form::radio("is_active","0",['checked=>checked'])}} inactive
                                @endif

                            </div>
                            <div class="form-group">
                                {{ Form::label("is drafted", null, ['class' => 'control-label']) }}
                                <br>
                                @if($brand->is_draft==1)
                                    {{Form::radio("is_draft","1",['checked=>checked'])}} yes

                                    {{Form::radio("is_draft","0")}} no
                                @else
                                    {{Form::radio("is_draft","1")}} yes

                                    {{Form::radio("is_draft","0",['checked=>checked'])}} no
                                @endif

                            </div>
                            <div class="form-group">
                                {{ Form::label("soft delete", null, ['class' => 'control-label']) }}
                                <br>
                                @if($brand->is_draft==1)
                                    {{Form::radio("soft_delete","1",['checked=>checked'])}} active

                                    {{Form::radio("soft_deletet","0")}} inactive
                                @else
                                    {{Form::radio("soft_delete","1")}} active

                                    {{Form::radio("soft_delete","0",['checked=>checked'])}} inactive
                                @endif

                            </div>

                            <!-- /.card-body -->

                            <div class="card-footer">
                                {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.card -->

