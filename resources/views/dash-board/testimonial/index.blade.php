@extends('layouts.backendmaster')


@include('elements.back-end.navbar')
@include('elements.back-end.sidebar')


{{--<div class="col-md-6">--}}

{{--    <table class="table table-hover table-striped">--}}


{{--<tr>--}}
{{--    <th>brand name</th>--}}
{{--    <th>brand title</th>--}}
{{--    <th>actions</th>--}}
{{--</tr>--}}
{{--        @foreach( $testimonialss as $key => $testimonials)--}}
{{--    <tr>--}}
{{--        <td>{{$testimonialss->firstItem()+ $key}}</td>--}}
{{--        <td>{{$testimonials->name}}</td>--}}
{{--        <td>{{$testimonials->title}}</td>--}}
{{--        <td>--}}
{{--            <a href="{{url("banners/$testimonials->id")}}">show</a>--}}
{{--            <a href="{{url("banners/$testimonials->id/edit")}}">edit</a>--}}
{{--            edit | delete | show--}}
{{--        </td>--}}
{{--    </tr>--}}
{{--            @endforeach--}}
{{--    </table>--}}
{{--    {{$testimonialss->links()}}--}}
{{--</div>--}}
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>DataTables</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('testimonial/create')}}">CREATE</a></li>
                            <li class="breadcrumb-item active">DataTables</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-auto">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">DataTable of testimonial</h3>
                        </div>
                        <div class="col-md-6">
                            @if($errors->any())
                                <div class="alert alert-danger">

                                    @foreach($errors->all() as $error)
                                        {{$error}}
                                    @endforeach


                                </div>
                            @endif

                            @if(session('message'))
                                <div class="alert alert-success">
                                    {{session('message')}}
                                </div>
                            @endif
                            <div class="wrapper">
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <table id="example2" class="table table-bordered table-responsive">
                                        <thead>
                                        <tr>
                                            <th> #</th>
                                            <th>ID</th>
                                            <th>name</th>
                                            <th>picture</th>
                                            <th>body</th>
                                            <th>designation</th>
                                            <th>is active</th>
                                            <th>is draft</th>
                                            <th>soft delete</th>
                                            <th>actions</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($testimonials as $key => $testimonial)
                                            <tr>
                                                {{--                                             <td>{{$testimonials->brand->name}}</td>--}}
                                                <td>{{$testimonials->firstItem()+ $key}}</td>
                                                <td>{{$testimonial->id}}</td>
                                                <td>{{$testimonial->name}}</td>
                                                <td><img src="{{asset("images/testimonials/$testimonial->picture")}}" alt="HTML5 Icon" style="width:150px;height:150px" ></td>
                                                <td>{{$testimonial->body}}</td>
                                                <td>{{$testimonial->designation}}</td>
                                                <td>{{$testimonial->is_active}}</td>
                                                <td>{{$testimonial->is_draft}}</td>
                                                <td>{{$testimonial->soft_delete}}</td>
                                                <td>
                                                    <a href="{{url("testimonial/$testimonial->id")}}" class="btn" type="submit">show</a>
                                                    <a href="{{url("testimonial/$testimonial->id/edit")}} " class="btn" type="submit">edit</a>
                                                    {!! Form::open(array('url' => "testimonial/$testimonial->id",'method' => 'delete' , 'class'=>'pull-right')) !!}
                                                    {{Form::submit('delete', ['class' =>'btn btn-danger'])}}

                                                    {!! Form::close() !!}

                                                </td>
                                            </tr>

                                        @endforeach


                                        </tbody>

                                    </table>
                                    {{$testimonials->links()}}
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @endsection;

