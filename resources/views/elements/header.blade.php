<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>winter</title>
    <link rel="icon" href="{{asset('img/favicon.png')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('front-end/css/bootstrap.min.css')}}">
    <!-- animate CSS -->
    <link rel="stylesheet" href="{{asset('front-end/css/animate.css')}}">
    <!-- owl carousel CSS -->
    <link rel="stylesheet" href="{{asset('front-end/css/owl.carousel.min.css')}}">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{asset('front-end/css/all.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/nice-select.css')}}">
    <!-- flaticon CSS -->
    <link rel="stylesheet" href="{{asset('front-end/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('front-end/css/themify-icons.css')}}">
    <!-- font awesome CSS -->
    <link rel="stylesheet" href="{{asset('front-end/css/magnific-popup.css')}}">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="{{asset('front-end/css/slick.css')}}">
    <!-- swiper CSS -->
    <link rel="stylesheet" href="{{asset('front-end/css/price_rangs.css')}}css/price_rangs.css">
    <!-- style CSS -->
    <link rel="stylesheet" href="{{asset('front-end/css/style.css')}}">



</head>

<body class="bg-white">
<!--::header part start::-->
<header class="main_menu home_menu">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-center">
            <div class="col-lg-11">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="navbar-brand" href="/"> <img src="{{asset('front-end/img/logo.png')}}" alt="logo"> </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span class="menu_icon"><i class="fas fa-bars"></i></span>
                    </button>

                    <div class="collapse navbar-collapse main-menu-item" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('home')}}">Home</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="" id="navbarDropdown_1"
                                   role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Shop
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown_1">
                                    <a class="dropdown-item" href="{{route('category.index')}}"> shop category</a>
                                    <a class="dropdown-item" href="{{route('single-product')}}">product details</a>

                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="{{route('blog')}}" id="navbarDropdown_3"
                                   role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    pages
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown_2">
                                    <a class="dropdown-item" href="{{route('checkout_login')}}">
                                        login

                                    </a>
                                    <a class="dropdown-item" href="{{route('tracking')}}">tracking</a>
                                    <a class="dropdown-item" href="{{route('checkout.index')}}">product checkout</a>
                                    <a class="dropdown-item" href="{{route('carts')}}">shopping cart</a>
                                    <a class="dropdown-item" href="{{route('confirmation')}}">confirmation</a>
                                    <a class="dropdown-item" href="{{route('elements')}}">elements</a>
                                </div>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="{{route('blog')}}" id="navbarDropdown_2"
                                   role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    blog
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown_2">
                                    <a class="dropdown-item" href="{{'blog'}}"> blog</a>
                                    <a class="dropdown-item" href="{{route('single-blog')}}">Single blog</a>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link" href="{{route('contacts')}}">Contact</a>
                            </li>
                            @guest
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                    <div class="dropdown shipping_details"
                    <div class="fa-shopping-cart">

                        <a href="{{route('carts')}}">
                        <i class="ti-bag" href="{{route('carts')}}"></i>
                        </a>
                        <span class="badge">{{\Gloudemans\Shoppingcart\Facades\Cart::content()->count()}}</span>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <div class="dropdown-item">
                                <h4 class="cart-title" style="color:white" >total ${{Cart::total()}}</h4>
                                    <a href="{{route('carts')}}">
                                        <div class="btn btn-secondary">
                                    <span class="text"> view cart</span>
                                        </div>
                                    </a>

                                </div>
                            </div>

                    </div>

{{--                    <div class="hearer_icon d-flex">--}}
{{--                        <div class="dropdown cart">--}}
{{--                            <a class="dropdown-toggle" href="#" id="" role="button"--}}
{{--                               data-toggle="dropdown" >--}}
{{--                                <i class="ti-bag"></i>--}}
{{--                            </a>--}}
{{--                            <!-- <div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
{{--                                <div class="single_product">--}}

{{--                                </div>--}}
{{--                            </div> -->--}}
{{--                        </div>--}}
{{--                        <a id="search_1" href="javascript:void(0)"><i class="ti-search"></i></a>--}}
{{--                    </div>--}}
                </nav>
            </div>
        </div>
    </div>
    <div class="search_input" id="search_input_box">
        <div class="container ">
            <form class="d-flex justify-content-between search-inner">
                <input type="text" class="form-control" id="search_input" placeholder="Search Here">
                <button type="submit" class="btn"></button>
                <span class="ti-close" id="close_search" title="Close Search"></span>
            </form>
        </div>
    </div>
</header>
