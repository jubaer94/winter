@extends('layouts.master')
@php
use Gloudemans\Shoppingcart\Facades\Cart
@endphp
@section('content')

  <!-- breadcrumb start-->
  <section class="breadcrumb breadcrumb_bg">
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-lg-12">
                  <div class="breadcrumb_iner">
                      <div class="breadcrumb_iner_item">
                          <p>Home/Shop/Single product/Cart list</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- breadcrumb start-->

  <!--================Cart Area =================-->
  <section class="cart_area section_padding">
    <div class="container">
      <div class="cart_inner">
        <div class="table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Product</th>
                  <th scope="col">delete</th>
                <th scope="col">Price</th>
                <th scope="col">Quantity</th>
                <th scope="col">Total</th>
              </tr>
            </thead>
            <tbody>

                @foreach(Cart::content() as $product)
                  <tr>
              <td>
                  <div class="media">
                    <div class="d-flex">
                      <img src="{{asset("images/productlist/".$product->model->picture)}}" alt="" />
                    </div>
                    <div class="media-body">
                      <p>{{$product->name}}</p>

                    </div>
                  </div>
                </td>
                      <td class="product-remove">
                       <a href="{{route('carts.delete',['id'=>$product->rowId])}}" class="product-del remove" title="remove this item" >
                  <button class="jsgrid-delete-button">delte item</button>
                       </a>
                      </td>
                <td>
                  <h5>{{$product->price}}</h5>
                </td>
                <td>
                  <div class="product_count">
                      <a href="{{url("carts/decr/$product->rowId/$product->qty")}}">

                      <span class="input-number-decrement"> <i class="ti-minus"></i></span>
                      </a>
                      <input class="input-number" type="text" value="{{$product->qty}}" >
                          <a href="{{url("carts/incr/$product->rowId/$product->qty")}}">
                      <span class="input-number-increment"> <i class="ti-plus"></i></span>
                      </a>
                  </div>
                </td>

                <td>
                  <h5>{{$product->total()}}</h5>
                </td>
                  </tr>
                  @endforeach

              <tr class="bottom_button">
                <td>
                  <a class="btn_1" href="#">Update Cart</a>
                </td>
                <td></td>
                <td></td>
                <td>
                  <div class="cupon_text float-right">
                    <a class="btn_1" href="#">Close Coupon</a>
                  </div>
                </td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td>
                  <h5>Subtotal</h5>
                </td>
                <td>
                  <h5>{{Cart::total()}}</h5>
                </td>
              </tr>
              <tr class="shipping_area">
                <td></td>
                <td></td>
                <td>
                  <h5>Shipping</h5>
                </td>
                <td>
                  <div class="shipping_box">
                    <ul class="list">
                      <li>
                        Flat Rate: $5.00
                        <input type="radio" aria-label="Radio button for following text input">
                      </li>
                      <li>
                        Free Shipping
                        <input type="radio" aria-label="Radio button for following text input">
                      </li>
                      <li>
                        Flat Rate: $10.00
                        <input type="radio" aria-label="Radio button for following text input">
                      </li>
                      <li class="active">
                        Local Delivery: $2.00
                        <input type="radio" aria-label="Radio button for following text input">
                      </li>
                    </ul>
                    <h6>
                      Calculate Shipping
                      <i class="fa fa-caret-down" aria-hidden="true"></i>
                    </h6>
                    <select class="shipping_select">
                      <option value="1">Bangladesh</option>
                      <option value="2">India</option>
                      <option value="4">Pakistan</option>
                    </select>
                    <select class="shipping_select section_bg">
                      <option value="1">Select a State</option>
                      <option value="2">Select a State</option>
                      <option value="4">Select a State</option>
                    </select>
                    <input class="post_code" type="text" placeholder="Postcode/Zipcode" />
                    <a class="btn_1" href="#">Update Details</a>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
          <div class="checkout_btn_inner float-right">
            <a class="btn_1" href="#">Continue Shopping</a>
            <a class="btn_1 checkout_btn_1" href="#">Proceed to checkout</a>
          </div>
        </div>
      </div>
  </section>
  <!--================End Cart Area =================-->

 @endsection
