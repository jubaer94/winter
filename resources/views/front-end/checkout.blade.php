@extends('layouts.master')

@section('content')


    <!--================Home Banner Area =================-->
  <!-- breadcrumb start-->
  <section class="breadcrumb breadcrumb_bg">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-lg-12">
          <div class="breadcrumb_iner">
            <div class="breadcrumb_iner_item">
              <p>Home / checkout</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- breadcrumb start-->

  <!--================Checkout Area =================-->
  <section class="checkout_area section_padding">
    <div class="container">
      <div class="cupon_area">
        <div class="check_title">
          <h2>
            Have a coupon?
            <a href="#">Click here to enter your code</a>
          </h2>
        </div>
        <input type="text" placeholder="Enter coupon code" />
        <a class="tp_btn" href="#">Apply Coupon</a>
      </div>
        @if(!empty($user->billinginfo->user_id))
      <div class="billing_details">
        <div class="row">
          <div class="col-lg-8">
            <h3>Billing Details</h3>
            <form class="row contact_form" action="#" method="post" novalidate="novalidate">
              <div class="col-md-6 form-group p_star">
                <h1>
                <span class="placeholder" data-placeholder="First name: {{$user->Billinginfo->first_name}}"></span>
                </h1>
              </div>

                <br>
              <div class="col-md-6 form-group p_star">
                  <h2><br></h2>
                  <h1>
                <span class="placeholder" data-placeholder="Last name: {{$user->Billinginfo->last_name}}"></span>
                  </h1>
              </div>
                <br>

                <br>
              <div class="col-md-6 form-group p_star">
                  <h2><br></h2>
                 <h1>

                <span class="placeholder" data-placeholder="Phone number :"></span>
                 </h1>
              </div>
                <br>
              <div class="col-md-12 form-group p_star">
                  <h2><br></h2>
                <h1>
                <span class="placeholder" data-placeholder="Email Address :{{$user->email}}"></span>
                </h1>
              </div>
                <br>
              <div class="col-md-12 form-group p_star">
                  <h2><br></h2>
                  <h1>
                  <span class="placeholder" data-placeholder="country :{{$user->Billinginfo->country}}"></span>
                  </h1>
              </div>
                <br>

              <div class="col-md-12 form-group p_star">
               <h2><br></h2>
                  <h1 style="">
                <span class="placeholder" data-placeholder="Address line:{{$user->Billinginfo->address}}"></span>
                  </h1>
              </div>
                <br>


              <div class="col-md-12 form-group">
                  <h2><br></h2>
                  <h1>
                  <span class="placeholder" data-placeholder="Email Address :{{$user->Billinginfo->zip}}"></span>

                  </h1>
              </div>
                <br>

              <div class="col-md-12 form-group">
                <div class="creat_account">
                  <h3>Shipping Details</h3>
                  <input type="checkbox" id="f-option3" name="selector" />
                  <label for="f-option3">Ship to a different address?</label>
                </div>
{{--                  {!! Form::open(array(url("billinginfo/{billinginfo}"),'method' => 'put')) !!}--}}
{{--                  {{Form::textarea('address',null,["class" => "form-control",--}}
{{--             "placeholder" => "different address ","id"=>"title"])}}--}}
{{--                  <div class="card-footer">--}}
{{--                      <button type="submit" class="btn btn-primary">Submit</button>--}}
{{--                      {{Form::submit('Submit',['class' => 'btn btn-primary'])}}--}}

{{--                  </div>--}}
{{--                  {!! Form::close() !!}--}}
              </div>
            </form>

          </div>
            @else
                <div class="btn">
                    <a class="btn_3" href="{{route('billinginfo.index')}}">Create your billing address to place your order</a>
                    <br>
                </div>
            @endif
          <div class="col-lg-4">
            <div class="order_box">
              <h2>Your Order</h2>
              <ul class="list">
                <li>
                  <a href="#">Product
                    <span>Total</span>
                  </a>
                </li>
                  @foreach(Cart::content() as $item)
                <li>
                  <a href="#">{{$item->name}}
                    <span class="middle">x {{$item->qty}}</span>
                    <span class="last">${{$item->total()}}</span>
                  </a>
                </li>
                  @endforeach

              </ul>
              <ul class="list list_2">
                <li>
                  <a href="#">Subtotal
                    <span>${{Cart::total()}}</span>
                  </a>
                </li>
                <li>
                  <a href="#">Shipping
                    <span>Flat rate: $50.00</span>
                  </a>
                </li>
                <li>
                  <a href="#">Total
                    <span>${{Cart::total()+50}}</span>
                  </a>
                </li>
              </ul>
                <div class='form-control total btn btn-info'>

                    <a href="{{route('payindex')}}">
                        Total:
                        <span class='amount'>${{Cart::total()+50}}</span>
                    </a>
                </div>
                <div class="payment_item">




          </div>
        </div>
      </div>
    </div>
  </section>



<div class='form-control total btn btn-info'>
    Total:
    <span class='amount'>$300</span>
</div>
<div>

</div>


  <!--================End Checkout Area =================-->

  <!-- subscribe_area part start-->

  <!--::subscribe_area part end::-->

@endsection

