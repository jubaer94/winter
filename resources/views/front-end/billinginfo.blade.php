@extends('layouts.master')

@section('content')

    <!-- Header part end-->

    <!--================Home Banner Area =================-->
    <!-- breadcrumb start-->

    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-6">
                        @if($errors->any())
                            <div class="alert alert-danger">

                                @foreach($errors->all() as $error)
                                    {{$error}}
                                @endforeach


                            </div>
                        @endif

                        @if(session('message'))
                            <div class="alert alert-success">
                                {{session('message')}}
                            </div>
                    @endif
                    <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Billing adress</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            {!! Form::open(array('url' => 'billinginfo','method' => 'post' , 'enctype'=>"multipart/form-data")) !!}
                            <div class="card-body">
                                <div class="form-group">

                                    {{Form::hidden('user_id',Auth::id())}}


                                </div>
                                <div class="form-group">

                                    {{ Form::label("first name", null, ['class' => 'control-label']) }}


                                    {{Form::text('first_name',null,["class" => "form-control",
                "placeholder" => "first name ","id"=>"title"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("last name", null, ['class' => 'control-label']) }}


                                    {{Form::text('last_name',null,["class" => "form-control",
                "placeholder" => "last name","id"=>"last_name"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("company_name", null, ['class' => 'control-label']) }}


                                    {{Form::text('company_name',null,["class" => "form-control",
                "placeholder" => "company_name","id"=>"company_name"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("address", null, ['class' => 'control-label']) }}


                                    {{Form::text('address',null,["class" => "form-control",
                "placeholder" => "address","id"=>"address"])}}

                                </div>


                                <div class="form-group">

                                    {{ Form::label("district", null, ['class' => 'control-label']) }}


                                    {{Form::text('district',null,["class" => "form-control",
                "placeholder" => "district","id"=>"district"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("country", null, ['class' => 'control-label']) }}


                                    {{Form::text('country',null,["class" => "form-control",
                "placeholder" => "country","id"=>"country"])}}

                                </div>
                                <div class="form-group">

                                    {{ Form::label("zipcode", null, ['class' => 'control-label']) }}


                                    {{Form::text('zipcode',null,["class" => "form-control",
                "placeholder" => "zipcode","id"=>"zipcode"])}}

                                </div>



                                <!-- /.card-body -->

                                <div class="card-footer">
                                    {{--                                <button type="submit" class="btn btn-primary">Submit</button>--}}
                                    {{Form::submit('Submit',['class' => 'btn btn-primary'])}}

                                </div>
                                {!! Form::close() !!}
                            </div>
                            <!-- /.card -->
                        </div>







        </section>
    </div>


    <!-- /.content -->
    </div>

    <!--::subscribe_area part end::-->

    <!--::footer_part start::-->
@endsection
