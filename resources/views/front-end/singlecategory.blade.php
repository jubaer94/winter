@extends('layouts.master')

@section('content')


    <!--================Home Banner Area =================-->
    <!-- breadcrumb start-->
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner">
                        <div class="breadcrumb_iner_item">
                            <p>Home / Category</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- breadcrumb start-->

    <!--================Category Product Area =================-->
    <section class="cat_product_area section_padding border_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="left_sidebar_area">
                        <aside class="left_widgets p_filter_widgets sidebar_box_shadow">
                            <div class="l_w_title">
                                <h3>Browse Categories</h3>
                            </div>
                            <div class="widgets_inner">
                                <ul class="list">
                                    <li>
                                        <a href="#">Fruits and Vegetables</a>
                                    </li>
                                    @foreach($categories as $category)
                                        <li>
                                            <a href="{{url("category/$category->id")}}">"{{$category->name}}"</a>
                                        </li>
                                    @endforeach
                                    <li class="sub-menu">
                                        <a href="#Electronics" class=" d-flex justify-content-between">
                                            Electronics
                                            <div class="right ti-plus"></div>
                                        </a>
                                        <ul>
                                            @foreach($categories as $category)
                                                <li>
                                                    <a href="{{url("category/$category->id")}}">"{{$category->name}}"</a>
                                                </li>
                                            @endforeach

                                        </ul>
                            </div>
                        </aside>

                        <aside class="left_widgets p_filter_widgets sidebar_box_shadow">
                            <div class="l_w_title">
                                <h3>Product filters</h3>
                            </div>
                            <div class="widgets_inner">
                                <ul class="list">
                                    <p>Brands</p>
                                    @foreach($brands as $brand)
                                    <li>
                                        <input type="radio" aria-label="Radio button for following text input">
                                        <a href="{{url("singlebrand/$brand->id")}}">{{$brand->title}}</a>
                                    </li>
                               @endforeach
                                </ul>
                                <ul class="list">
                                    <p>color</p>
                                    <li>
                                        <input type="radio" aria-label="Radio button for following text input">
                                        <a href="#">Black</a>
                                    </li>
                                    <li>
                                        <input type="radio" aria-label="Radio button for following text input">
                                        <a href="#">Black Leather</a>
                                    </li>
                                    <li>
                                        <input type="radio" aria-label="Radio button for following text input">
                                        <a href="#">Black with red</a>
                                    </li>
                                    <li>
                                        <input type="radio" aria-label="Radio button for following text input">
                                        <a href="#">Gold</a>
                                    </li>
                                    <li>
                                        <input type="radio" aria-label="Radio button for following text input">
                                        <a href="#">Spacegrey</a>
                                    </li>
                                </ul>
                            </div>
                        </aside>

                        <aside class="left_widgets p_filter_widgets price_rangs_aside sidebar_box_shadow">
                            <div class="l_w_title">
                                <h3>Price Filter</h3>
                            </div>
                            <div class="widgets_inner">
                                <div class="range_item">
                                    <!-- <div id="slider-range"></div> -->
                                    <input type="text" class="js-range-slider" value="" />
                                    <div class="d-flex align-items-center">
                                        <div class="price_text">
                                            <p>Price :</p>
                                        </div>
                                        <div class="price_value d-flex justify-content-center">
                                            <input type="text" class="js-input-from" id="amount" readonly />
                                            <span>to</span>
                                            <input type="text" class="js-input-to" id="amount" readonly />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </aside>
                    </div>
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="product_top_bar d-flex justify-content-between align-items-center">
                                <div class="single_product_menu product_bar_item">
                                    <h2>{{$categoryname->name}}</h2>
                                </div>
                                <div class="product_top_bar_iner product_bar_item d-flex">
                                    <div class="product_bar_single">
                                        <select class="wide">
                                            <option data-display="Default sorting">Default sorting</option>
                                            <option value="1">Some option</option>
                                            <option value="2">Another option</option>
                                            <option value="3">Potato</option>
                                        </select>
                                    </div>
                                    <div class="product_bar_single">
                                        <select>
                                            <option data-display="Show 12">Show 12</option>
                                            <option value="1">Show 20</option>
                                            <option value="2">Show 30</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @foreach($products as $product)
                            <div class="col-lg-4 col-sm-6">
                                <div class="single_category_product">
                                    <div class="single_category_img">
                                        <img src="{{asset("images/productlist/$product->picture")}}" alt="">
                                        <div class="category_social_icon">
                                            <ul>
                                                <li><a href="#"><i class="ti-heart"></i></a></li>

                                                <li>
                                                    <form action="{{url("Addtocart/$product->id")}}" method="POST">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="qty" value="1" />
                                                    <a href="#" onclick="this.parentNode.submit()"><i class="ti-bag"></i></a>
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="category_product_text">
                                            <a href={{url("singleproduct/$product->id")}}><h5>{{$product->name}}</h5></a>
                                            <p>${{$product->mrp}}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
        </div>
    </section>
    <!--================End Category Product Area =================-->

    <!-- free shipping here -->
    <section class="shipping_details section_padding border_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-sm-6">
                    <div class="single_shopping_details">
                        <img src="{{asset('front-end/img/icon/icon_1.png')}}" alt="">
                        <h4>Free shipping</h4>
                        <p>Divided face for bearing the divide unto seed winged divided light Forth.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_shopping_details">
                        <img src="{{asset('front-end/img/icon/icon_2.png')}}" alt="">
                        <h4>Free shipping</h4>
                        <p>Divided face for bearing the divide unto seed winged divided light Forth.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_shopping_details">
                        <img src="{{asset('front-end/img/icon/icon_3.png')}}" alt="">
                        <h4>Free shipping</h4>
                        <p>Divided face for bearing the divide unto seed winged divided light Forth.</p>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single_shopping_details">
                        <img src="{{asset('front-end/img/icon/icon_4.png')}}" alt="">
                        <h4>Free shipping</h4>
                        <p>Divided face for bearing the divide unto seed winged divided light Forth.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- free shipping end -->

    <!-- subscribe_area part start-->
    <section class="instagram_photo">
        <div class="container-fluid>
            <div class="row">
        <div class="col-lg-12">
            <div class="instagram_photo_iner">
                <div class="single_instgram_photo">
                    <img src="{{asset('front-end/img/instagram/inst_1.png')}}" alt="">
                    <a href="#"><i class="ti-instagram"></i></a>
                </div>
                <div class="single_instgram_photo">
                    <img src="{{asset('front-end/img/instagram/inst_2.png')}}" alt="">
                    <a href="#"><i class="ti-instagram"></i></a>
                </div>
                <div class="single_instgram_photo">
                    <img src="{{asset('front-end/img/instagram/inst_3.png')}}" alt="">
                    <a href="#"><i class="ti-instagram"></i></a>
                </div>
                <div class="single_instgram_photo">
                    <img src="{{asset('front-end/img/instagram/inst_4.png')}}" alt="">
                    <a href="#"><i class="ti-instagram"></i></a>
                </div>
                <div class="single_instgram_photo">
                    <img src="{{asset('front-end/img/instagram/inst_5.png')}}" alt="">
                    <a href="#"><i class="ti-instagram"></i></a>
                </div>
            </div>
        </div>
        </div>
        </div>
    </section>
    <!--::subscribe_area part end::-->

@endsection
